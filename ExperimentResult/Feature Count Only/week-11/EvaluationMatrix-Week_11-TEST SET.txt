=== Confusion Matrix ===

   a   b   c   <-- classified as
 146   1   0 |   a = Green
  41   9   4 |   b = Yellow
  23   8  14 |   c = Red
