=== Confusion Matrix ===

   a   b   c   <-- classified as
 129  17   1 |   a = Green
  30  21   3 |   b = Yellow
  13  11  21 |   c = Red
