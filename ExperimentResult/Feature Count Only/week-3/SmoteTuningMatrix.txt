NN = 1
   a   b   c   <-- classified as
 127  67  18 |   a = Green
  16  10   6 |   b = Yellow
   5   6  16 |   c = Red

NN = 2
   a   b   c   <-- classified as
 124  66  22 |   a = Green
  18   8   6 |   b = Yellow
   5   4  18 |   c = Red

NN = 3
   a   b   c   <-- classified as
 118  75  19 |   a = Green
  17   7   8 |   b = Yellow
   5   5  17 |   c = Red

NN = 4
   a   b   c   <-- classified as
 114  79  19 |   a = Green
  19   7   6 |   b = Yellow
   5   4  18 |   c = Red

NN = 5
   a   b   c   <-- classified as
 112  77  23 |   a = Green
  19   8   5 |   b = Yellow
   6   4  17 |   c = Red

NN = 6
   a   b   c   <-- classified as
 118  73  21 |   a = Green
  18   8   6 |   b = Yellow
   8   2  17 |   c = Red

NN = 7
   a   b   c   <-- classified as
 124  68  20 |   a = Green
  20   6   6 |   b = Yellow
   5   4  18 |   c = Red

NN = 8
   a   b   c   <-- classified as
 119  72  21 |   a = Green
  19   7   6 |   b = Yellow
   6   4  17 |   c = Red

NN = 9
   a   b   c   <-- classified as
 117  75  20 |   a = Green
  19   7   6 |   b = Yellow
   6   2  19 |   c = Red

NN = 10
   a   b   c   <-- classified as
 113  79  20 |   a = Green
  19   7   6 |   b = Yellow
   6   2  19 |   c = Red

NN = 11
   a   b   c   <-- classified as
 116  78  18 |   a = Green
  18   7   7 |   b = Yellow
   4   5  18 |   c = Red

NN = 12
   a   b   c   <-- classified as
 118  76  18 |   a = Green
  20   6   6 |   b = Yellow
   4   6  17 |   c = Red

NN = 13
   a   b   c   <-- classified as
 116  76  20 |   a = Green
  19   5   8 |   b = Yellow
   6   4  17 |   c = Red

NN = 14
   a   b   c   <-- classified as
 116  79  17 |   a = Green
  18   8   6 |   b = Yellow
   6   4  17 |   c = Red

NN = 15
   a   b   c   <-- classified as
 113  81  18 |   a = Green
  17   9   6 |   b = Yellow
   5   5  17 |   c = Red

NN = 16
   a   b   c   <-- classified as
 122  75  15 |   a = Green
  19   6   7 |   b = Yellow
   5   4  18 |   c = Red

NN = 17
   a   b   c   <-- classified as
 120  73  19 |   a = Green
  17   9   6 |   b = Yellow
   5   3  19 |   c = Red

NN = 18
   a   b   c   <-- classified as
 114  77  21 |   a = Green
  18   8   6 |   b = Yellow
   6   4  17 |   c = Red

NN = 19
   a   b   c   <-- classified as
 117  81  14 |   a = Green
  18   8   6 |   b = Yellow
   6   4  17 |   c = Red

NN = 20
   a   b   c   <-- classified as
 128  70  14 |   a = Green
  20   6   6 |   b = Yellow
   5   2  20 |   c = Red

NN = 21
   a   b   c   <-- classified as
 116  75  21 |   a = Green
  17   9   6 |   b = Yellow
   6   3  18 |   c = Red

NN = 22
   a   b   c   <-- classified as
 119  75  18 |   a = Green
  20   5   7 |   b = Yellow
   7   2  18 |   c = Red

NN = 23
   a   b   c   <-- classified as
 117  78  17 |   a = Green
  16  10   6 |   b = Yellow
   5   5  17 |   c = Red

