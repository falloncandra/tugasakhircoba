=== Confusion Matrix ===

  a  b  c   <-- classified as
 92 40 15 |  a = Green
 14 24 16 |  b = Yellow
 15  7 23 |  c = Red
