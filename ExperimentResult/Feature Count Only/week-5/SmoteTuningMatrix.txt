NN = 1
   a   b   c   <-- classified as
 133  57  22 |   a = Green
  13  13   6 |   b = Yellow
   5   5  17 |   c = Red

NN = 2
   a   b   c   <-- classified as
 136  54  22 |   a = Green
  14  12   6 |   b = Yellow
   5   4  18 |   c = Red

NN = 3
   a   b   c   <-- classified as
 137  59  16 |   a = Green
  12  13   7 |   b = Yellow
   3   6  18 |   c = Red

NN = 4
   a   b   c   <-- classified as
 130  64  18 |   a = Green
  15  13   4 |   b = Yellow
   5   3  19 |   c = Red

NN = 5
   a   b   c   <-- classified as
 126  64  22 |   a = Green
  14  11   7 |   b = Yellow
   4   5  18 |   c = Red

NN = 6
   a   b   c   <-- classified as
 132  63  17 |   a = Green
  14  12   6 |   b = Yellow
   5   3  19 |   c = Red

NN = 7
   a   b   c   <-- classified as
 136  55  21 |   a = Green
  14  14   4 |   b = Yellow
   5   4  18 |   c = Red

NN = 8
   a   b   c   <-- classified as
 142  50  20 |   a = Green
  14  12   6 |   b = Yellow
   5   4  18 |   c = Red

NN = 9
   a   b   c   <-- classified as
 134  60  18 |   a = Green
  13  12   7 |   b = Yellow
   5   3  19 |   c = Red

NN = 10
   a   b   c   <-- classified as
 135  59  18 |   a = Green
  15  12   5 |   b = Yellow
   4   4  19 |   c = Red

NN = 11
   a   b   c   <-- classified as
 128  65  19 |   a = Green
  13  14   5 |   b = Yellow
   5   4  18 |   c = Red

NN = 12
   a   b   c   <-- classified as
 129  60  23 |   a = Green
  16  10   6 |   b = Yellow
   6   2  19 |   c = Red

NN = 13
   a   b   c   <-- classified as
 136  59  17 |   a = Green
  14  13   5 |   b = Yellow
   5   5  17 |   c = Red

NN = 14
   a   b   c   <-- classified as
 128  64  20 |   a = Green
  13  12   7 |   b = Yellow
   4   4  19 |   c = Red

NN = 15
   a   b   c   <-- classified as
 134  61  17 |   a = Green
  13  14   5 |   b = Yellow
   5   4  18 |   c = Red

NN = 16
   a   b   c   <-- classified as
 137  57  18 |   a = Green
  16  11   5 |   b = Yellow
   5   2  20 |   c = Red

NN = 17
   a   b   c   <-- classified as
 139  58  15 |   a = Green
  14  14   4 |   b = Yellow
   5   3  19 |   c = Red

NN = 18
   a   b   c   <-- classified as
 139  52  21 |   a = Green
  14  13   5 |   b = Yellow
   5   4  18 |   c = Red

NN = 19
   a   b   c   <-- classified as
 135  62  15 |   a = Green
  16  13   3 |   b = Yellow
   5   3  19 |   c = Red

NN = 20
   a   b   c   <-- classified as
 142  54  16 |   a = Green
  17  10   5 |   b = Yellow
   5   3  19 |   c = Red

NN = 21
   a   b   c   <-- classified as
 138  58  16 |   a = Green
  16  11   5 |   b = Yellow
   5   3  19 |   c = Red

NN = 22
   a   b   c   <-- classified as
 137  60  15 |   a = Green
  14  13   5 |   b = Yellow
   4   5  18 |   c = Red

NN = 23
   a   b   c   <-- classified as
 140  58  14 |   a = Green
  13  13   6 |   b = Yellow
   4   3  20 |   c = Red

