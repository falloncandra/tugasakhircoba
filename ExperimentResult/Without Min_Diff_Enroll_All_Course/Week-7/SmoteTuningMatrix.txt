NN = 1
   a   b   c   <-- classified as
 140  59  13 |   a = Green
  17  10   5 |   b = Yellow
   3   5  19 |   c = Red

NN = 2
   a   b   c   <-- classified as
 141  60  11 |   a = Green
  17  10   5 |   b = Yellow
   3   6  18 |   c = Red

NN = 3
   a   b   c   <-- classified as
 125  76  11 |   a = Green
  14  11   7 |   b = Yellow
   3   3  21 |   c = Red

NN = 4
   a   b   c   <-- classified as
 137  66   9 |   a = Green
  14  10   8 |   b = Yellow
   3   6  18 |   c = Red

NN = 5
   a   b   c   <-- classified as
 136  58  18 |   a = Green
  17  11   4 |   b = Yellow
   3   5  19 |   c = Red

NN = 6
   a   b   c   <-- classified as
 138  59  15 |   a = Green
  17  11   4 |   b = Yellow
   3   5  19 |   c = Red

NN = 7
   a   b   c   <-- classified as
 131  67  14 |   a = Green
  13  13   6 |   b = Yellow
   3   5  19 |   c = Red

NN = 8
   a   b   c   <-- classified as
 136  63  13 |   a = Green
  18  10   4 |   b = Yellow
   3   5  19 |   c = Red

NN = 9
   a   b   c   <-- classified as
 140  59  13 |   a = Green
  16  11   5 |   b = Yellow
   3   5  19 |   c = Red

NN = 10
   a   b   c   <-- classified as
 135  65  12 |   a = Green
  13  13   6 |   b = Yellow
   4   4  19 |   c = Red

NN = 11
   a   b   c   <-- classified as
 139  62  11 |   a = Green
  14  12   6 |   b = Yellow
   3   5  19 |   c = Red

NN = 12
   a   b   c   <-- classified as
 127  74  11 |   a = Green
  17  10   5 |   b = Yellow
   2   6  19 |   c = Red

NN = 13
   a   b   c   <-- classified as
 143  58  11 |   a = Green
  15  13   4 |   b = Yellow
   3   6  18 |   c = Red

NN = 14
   a   b   c   <-- classified as
 132  67  13 |   a = Green
  16  11   5 |   b = Yellow
   2   6  19 |   c = Red

NN = 15
   a   b   c   <-- classified as
 134  67  11 |   a = Green
  17  11   4 |   b = Yellow
   2   7  18 |   c = Red

NN = 16
   a   b   c   <-- classified as
 139  62  11 |   a = Green
  16  13   3 |   b = Yellow
   3   6  18 |   c = Red

NN = 17
   a   b   c   <-- classified as
 137  61  14 |   a = Green
  17  10   5 |   b = Yellow
   3   5  19 |   c = Red

NN = 18
   a   b   c   <-- classified as
 128  70  14 |   a = Green
  14  12   6 |   b = Yellow
   3   6  18 |   c = Red

NN = 19
   a   b   c   <-- classified as
 136  67   9 |   a = Green
  16  12   4 |   b = Yellow
   3   5  19 |   c = Red

NN = 20
   a   b   c   <-- classified as
 143  59  10 |   a = Green
  18  11   3 |   b = Yellow
   3   4  20 |   c = Red

NN = 21
   a   b   c   <-- classified as
 144  58  10 |   a = Green
  15  12   5 |   b = Yellow
   3   4  20 |   c = Red

NN = 22
   a   b   c   <-- classified as
 144  59   9 |   a = Green
  16  10   6 |   b = Yellow
   3   6  18 |   c = Red

NN = 23
   a   b   c   <-- classified as
 135  70   7 |   a = Green
  16  13   3 |   b = Yellow
   3   6  18 |   c = Red

