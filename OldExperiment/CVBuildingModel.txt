//Building model
		Logistic log = new Logistic();
		log.setRidge(0.08); //Cfs SE
		
		//randoming data
		int folds = 10;
					
		//stratify
		if(train.classAttribute().isNominal())
			train.stratify(folds);
		
		//0 -> mean Precision
		//1 -> meanF1
		//1 -> meanF2
		//1 -> meanF3
		
		double[] testEvalRes = new double[5];
		double[] trainEvalRes = new double[5];
		double[] allEvalRes = new double[5];
				
		//String[][] matrixConf= new String[3][3];
		
		
		for(int n=0; n<folds; n++)
		{			
			Instances trainCV = train.trainCV(folds, n);
			Instances testCV = train.testCV(folds, n);
			
			Evaluation evalTest = new Evaluation(trainCV);
			Evaluation evalTrain = new Evaluation(trainCV);
			Evaluation evalAll = new Evaluation(trainCV);
			
			log.buildClassifier(trainCV);
			
			evalTest.evaluateModel(log, testCV);
			evalTrain.evaluateModel(log, trainCV);
			evalAll.evaluateModel(log, newDataFull);
							
			testEvalRes[0]+=evalTest.pctCorrect();
			testEvalRes[1] += evalTest.fMeasure(0);
			testEvalRes[2] += evalTest.fMeasure(1);
			testEvalRes[3] += evalTest.fMeasure(2);
			testEvalRes[4] += ((evalTest.fMeasure(0)+evalTest.fMeasure(1)+evalTest.fMeasure(2))/3.0);
			
			trainEvalRes[0]+=evalTrain.pctCorrect();
			trainEvalRes[1] += evalTrain.fMeasure(0);
			trainEvalRes[2] += evalTrain.fMeasure(1);
			trainEvalRes[3] += evalTrain.fMeasure(2);
			trainEvalRes[4] += ((evalTrain.fMeasure(0)+evalTrain.fMeasure(1)+evalTrain.fMeasure(2))/3.0);
			
			allEvalRes[0]+=evalAll.pctCorrect();
			allEvalRes[1] += evalAll.fMeasure(0);
			allEvalRes[2] += evalAll.fMeasure(1);
			allEvalRes[3] += evalAll.fMeasure(2);
			allEvalRes[4] += ((evalAll.fMeasure(0)+evalAll.fMeasure(1)+evalAll.fMeasure(2))/3.0);
		}
		
		testEvalRes[0]=testEvalRes[0]/folds;
		testEvalRes[1]=testEvalRes[1]/folds;
		testEvalRes[2]=testEvalRes[2]/folds;
		testEvalRes[3]=testEvalRes[3]/folds;
		testEvalRes[4]=testEvalRes[4]/folds;
		
		trainEvalRes[0]=trainEvalRes[0]/folds;
		trainEvalRes[1]=trainEvalRes[1]/folds;
		trainEvalRes[2]=trainEvalRes[2]/folds;
		trainEvalRes[3]=trainEvalRes[3]/folds;
		trainEvalRes[4]=trainEvalRes[4]/folds;
		
		allEvalRes[0]=allEvalRes[0]/folds;
		allEvalRes[1]=allEvalRes[1]/folds;
		allEvalRes[2]=allEvalRes[2]/folds;
		allEvalRes[3]=allEvalRes[3]/folds;
		allEvalRes[4]=allEvalRes[4]/folds;
		
		System.out.println("===================================TEST SET===========================================");
		System.out.println("Correct % = "+testEvalRes[0]);
		
		System.out.println("FMeasure"+firstClass+" = "+testEvalRes[1]);
		System.out.println("FMeasure "+secondClass+" = "+testEvalRes[2]);
		System.out.println("FMeasure "+thirdClass+" = "+testEvalRes[3]);
		System.out.println("Mean FMeasure = "+(testEvalRes[1]+testEvalRes[2]+testEvalRes[3])/3.0);
		
		System.out.println("===================================TRAIN SET===========================================");
		System.out.println("Correct % = "+trainEvalRes[0]);
		
		System.out.println("FMeasure "+firstClass+" = "+trainEvalRes[1]);
		System.out.println("FMeasure "+secondClass+" = "+trainEvalRes[2]);
		System.out.println("FMeasure = "+thirdClass+" "+trainEvalRes[3]);
		System.out.println("Mean FMeasure = "+(trainEvalRes[1]+trainEvalRes[2]+trainEvalRes[3])/3.0);
		
		System.out.println("===================================ALL SET===========================================");
		System.out.println("Correct % = "+allEvalRes[0]);
		
		System.out.println("FMeasure "+firstClass+" = "+allEvalRes[1]);
		System.out.println("FMeasure "+secondClass+" = "+allEvalRes[2]);
		System.out.println("FMeasure "+thirdClass+" = "+allEvalRes[3]);
		System.out.println("Mean FMeasure = "+(allEvalRes[1]+allEvalRes[2]+allEvalRes[3])/3.0);