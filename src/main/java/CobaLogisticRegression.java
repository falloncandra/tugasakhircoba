import java.io.File;
import java.io.IOException;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.CSVSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

//coba logistic regression dengan file training dan test set yang sudah disediakan

public class CobaLogisticRegression
{
	public static void main(String[] args) throws Exception
	{
		//get the path of this code
		String relPath = new File("").getAbsolutePath();
		relPath = relPath + "\\";
		System.out.println(relPath);
					
		//name of the input files
		String fileInput = "data-student-learning-analytics-plus-class.csv";
		String fileTrain = "data-student-learning-analytics-plus-class-train.csv";
		String fileTest = "data-student-learning-analytics-plus-class-test.csv";
		//String fileOutput = "data-student-learning-analytics-plus-class.arff";
		
		//load complete data
		CSVLoader loader = new CSVLoader();
		loader.setSource(new File(relPath+fileInput));
		Instances data = loader.getDataSet();
		
		//load training data
		loader.setSource(new File(relPath+fileTrain));
		Instances dataTrain = loader.getDataSet();
		
		//load testing data
		loader.setSource(new File(relPath+fileTest));
		Instances dataTest = loader.getDataSet();
		
		Remove remover = new Remove();
		//indeks atribut mulai dari 1
		remover.setAttributeIndices("1,2,3,6,7,8,9");
		remover.setInvertSelection(false);
		
		remover.setInputFormat(data);
		Instances newData = Filter.useFilter(data, remover);
		newData.setClassIndex(newData.numAttributes()-1);
		
		remover.setInputFormat(dataTrain);
		Instances newDataTrain = Filter.useFilter(dataTrain, remover);
		newDataTrain.setClassIndex(newDataTrain.numAttributes()-1);
		
		remover.setInputFormat(dataTest);
		Instances newDataTest = Filter.useFilter(dataTest, remover);
		newDataTest.setClassIndex(newDataTest.numAttributes()-1);
				
		Logistic log = new Logistic();
		log.buildClassifier(newDataTrain);
		
		Evaluation eval = new Evaluation(newDataTrain);
		eval.evaluateModel(log, newDataTest);
		
		System.out.println("Correct % = "+eval.pctCorrect());
		System.out.println("Incorrect % = "+eval.pctIncorrect());
		System.out.println("AUC = "+eval.areaUnderROC(1));
		System.out.println("kappa = "+eval.kappa());
		System.out.println("MAE = "+eval.meanAbsoluteError());
		System.out.println("RMSE = "+eval.rootMeanSquaredError());
		System.out.println("RAE = "+eval.relativeAbsoluteError());
		System.out.println("RRSE = "+eval.rootRelativeSquaredError());
		System.out.println("Precision = "+eval.precision(1));
		System.out.println("Recall = "+eval.recall(1));
		System.out.println("FMeasure = "+eval.fMeasure(1));
		System.out.println("Error Rate = "+eval.errorRate());
		System.out.println(eval.toMatrixString());
		
	}
		
}
