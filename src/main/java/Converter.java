import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.CSVSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.ArffSaver;

import java.io.File;
import java.io.IOException;

public class Converter 
{

	public void ARFFToCSV(String inputPath, String outputPath) throws IOException
	{
		System.out.println(inputPath);
		System.out.println(outputPath);
		
		ArffLoader loader = new ArffLoader();
		loader.setSource(new File(inputPath));
		Instances data = loader.getDataSet();
		
		CSVSaver saver = new CSVSaver();
		saver.setInstances(data);
		
		saver.setFile(new File(outputPath));
		saver.writeBatch();
		
	}
	
	public void CSVToARFF(String inputPath, String outputPath)throws IOException
	{
		System.out.println(inputPath);
		System.out.println(outputPath);
		
		CSVLoader loader = new CSVLoader();
		loader.setSource(new File(inputPath));
		Instances data = loader.getDataSet();
		
		ArffSaver saver = new ArffSaver();
		saver.setInstances(data);
		
		saver.setFile(new File(outputPath));
		saver.writeBatch();
	}

}
