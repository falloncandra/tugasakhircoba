import java.io.File;
import java.io.IOException;

//coba convert dari arff ke csv dan sebaliknya

public class ConverterTest
{
	public static void main (String[] args) throws IOException
	{
		Converter converter = new Converter();
		
		String relPath = new File("").getAbsolutePath();
		relPath = relPath + "\\";
		System.out.println(relPath);
		
	
		
//		String fileInputArff = "data-student-learning-analytics-plus-class.arff";
//		String fileOutputCSV = "data-student-learning-analytics-plus-class.csv";
//				
		String fileInputCSV = "DummyData\\SouthAfricanRemoved.csv";
		String fileOutputArff = "DummyData\\SouthAfricanRemoved.arff";
		
	
		try
		{
			
		//	converter.ARFFToCSV(relPath+fileInputArff, relPath+fileOutputCSV);
			
			converter.CSVToARFF(relPath+fileInputCSV, relPath+fileOutputArff);
		}
		catch (IOException e)
		{
			
		}	
		
	}
}
