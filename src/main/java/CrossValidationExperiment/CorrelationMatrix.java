package CrossValidationExperiment;

import weka.core.Instances;
import weka.core.Utils;

public class CorrelationMatrix
{
	
	private Instances dataset;
	
	public CorrelationMatrix(Instances dataset)
	{
		this.dataset =  dataset;
	}
	
	public double[][] getCorrelationMatrix()
	{
		int numAtt = dataset.numAttributes()-1;
		int numInst = dataset.numInstances();
		
		double[][] corMat = new double[numAtt][numAtt];
		
		for(int i=0;i<numAtt;i++)
		{
			for(int j=0;j<numAtt;j++)
			{
				if(i != j)
				{
					double[] att1 = dataset.attributeToDoubleArray(i);
					double[] att2 = dataset.attributeToDoubleArray(j);
					
					double corr = Utils.correlation(att1, att2, numInst);
					corMat[i][j] = corr;
				}
				else
					corMat[i][j]=100;
			}
		}
		
		return corMat;
	}
}
