package CrossValidationExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.Standardize;
import weka.filters.unsupervised.instance.Randomize;

public class CrossValExpGYR 
{
	public static void main (String args[]) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
							
		//name of the input files
		String fileInput = "data-student-learning-analytics-plus-class.csv";	
		
		//load data
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[4];
		options[0]="-N";
		options[1]="last";
		options[2]="-L";
		options[3]="last:Green,Yellow,Red";
		loader.setOptions(options);
		
		loader.setSource(new File(path+fileInput));
		Instances dataFull = loader.getDataSet();
		dataFull.setClassIndex(dataFull.numAttributes()-1);
		
		
		CSVSaver saver = new CSVSaver();
		ArffSaver saverArff = new ArffSaver();
		
					
		//remove unnecessary attributes (att starts from 1)
		Remove remover = new Remove();
		remover.setAttributeIndices("1,2,3,4,5,6,7,8,9,10,11");
		remover.setInvertSelection(false);
		remover.setInputFormat(dataFull);
		Instances newDataFull = Filter.useFilter(dataFull, remover);
		
	
		//split data to train, test set (80,20)
		DataSplitterCV splitter = new DataSplitterCV(newDataFull);
		splitter.splitData();
		
		Instances train = splitter.getTrainSet();
		Instances test = splitter.getTestSet();
		
		String firstClass = train.classAttribute().value(0);
		String secondClass = train.classAttribute().value(1);
		String thirdClass = train.classAttribute().value(2);
		
//		saver.setInstances(train);
//		saver.setFile(new File("trainNoSmote-GYR80.csv"));
//		saver.writeBatch();
//	
//		saver.setInstances(test);
//		saver.setFile(new File("testNoSmote-GYR80.csv"));
//		saver.writeBatch();	
		
		
		//get the majority class
		AttributeStats stats = train.attributeStats(train.classIndex());
		int[] classMember = stats.nominalCounts;
		
		int majorClass = 0;
			
		for(int i=1;i<classMember.length;i++)
		{
			if(classMember[i]>classMember[majorClass])
			{
				majorClass=i;				
			}
		}
		
//========================================= SMOTE to handle imbalanced class
		//NN GYR= 20
		SMOTE smoter = new SMOTE();
		smoter.setRandomSeed(161803398);
		smoter.setNearestNeighbors(20); //result from parameter tuning using default LR
		
		for(int i=0;i<classMember.length;i++)
		{
			if(i!=majorClass)
			{
				int percentage = (classMember[majorClass]/classMember[i]-1)*100;
				//int percentage = (classMember[majorClass]/classMember[i])*100;
				smoter.setInputFormat(train);
				smoter.setPercentage(percentage);
				smoter.setClassValue((i+1)+"");
				train = Filter.useFilter(train, smoter);
			}
		}
	
//========================================= Randomize
		Randomize randomizer = new Randomize();
		randomizer.setRandomSeed(161803398);
		randomizer.setInputFormat(train);
		train = Filter.useFilter(train, randomizer);	

//========================================= Standardize (Z-Score)
		Standardize standardizer = new Standardize();
		standardizer.setInputFormat(train);
		train = Filter.useFilter(train, standardizer);
		test = Filter.useFilter(test, standardizer);
		newDataFull = Filter.useFilter(newDataFull, standardizer);
		
//		saverArff.setInstances(train);
//		saverArff.setFile(new File(path+"trainStandardized-GYR80.arff"));
//		saverArff.writeBatch();
//		
//		saverArff.setInstances(test);
//		saverArff.setFile(new File(path+"testStandardized-GYR80.arff"));
//		saverArff.writeBatch();
		
		
//		saver.setInstances(train);
//		saver.setFile(new File("train-RegTune-All-GYR80.csv"));
//		saver.writeBatch();
//	
//		saver.setInstances(test);
//		saver.setFile(new File("test-RegTune-All-GYR80.csv"));
//		saver.writeBatch();	
//		
		
//=========================================Attribute Selection
		//String retainedAtt = "1,2,5,6,7,9,11,14,15,16,17,18,19,20,22,"+(train.classIndex()+1);//consistency SE
		
		//String retainedAtt = "1,4,5,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,"+(train.classIndex()+1);//cfs SE
		
		//removed Att in R = 2,4,5,6,7,8,10,14,15,17,18,19,24
		//retained Att in R = 1,3,9,11,12,13,16,20,21,22,23,+class
		
//		remover.setAttributeIndices(retainedAtt);
//		remover.setInvertSelection(true);
//				
//		remover.setInputFormat(train);
//		train = Filter.useFilter(train, remover);
//		train.setClassIndex(train.numAttributes()-1);
//		
//		remover.setInputFormat(test);
//		test = Filter.useFilter(test, remover);
//		
//		remover.setInputFormat(newDataFull);
//		newDataFull = Filter.useFilter(newDataFull, remover);

//========================================= Remove Att
		
//	Instances saveTrain = new Instances(train);
//	Instances cobaTrain = new Instances(train);
//	
//	Instances saveTest = new Instances(test);
//	Instances cobaTest = new Instances(test);
//	
//	Instances saveAll = new Instances(newDataFull);
//	Instances cobaAll = new Instances(newDataFull);
	
//	for(int t=1; t<25;t++)
//	{
//		train = new Instances(cobaTrain);
//		test = new Instances(cobaTest);
//		newDataFull = new Instances(cobaAll);
//		System.out.println();
//		System.out.println(t);
		
		//String removedAtt = t+"";
//		String removedAtt = "6,8,9";
//		remover.setAttributeIndices(removedAtt);
//		remover.setInvertSelection(false);
//				
//		remover.setInputFormat(train);
//		train = Filter.useFilter(train, remover);
//		train.setClassIndex(train.numAttributes()-1);
//		
//		remover.setInputFormat(test);
//		test = Filter.useFilter(test, remover);
//		
//		remover.setInputFormat(newDataFull);
//		newDataFull = Filter.useFilter(newDataFull, remover);
////		
//
//		CorrelationMatrix corMatGen = new CorrelationMatrix(train);
//		double[][] corMat = corMatGen.getCorrelationMatrix();
//		
//		String corMatFile = "CorrMat-GYR80.csv";
//		BufferedWriter outCorMat = new BufferedWriter(new FileWriter(new File(corMatFile)));
//		outCorMat.write("AttName,");
//		for(int i=0;i<corMat.length;i++)
//		{
//			if(i != corMat[0].length-1)
//				outCorMat.write(train.attribute(i).name()+",");
//			else
//				outCorMat.write(train.attribute(i).name()+"\n");
//		}
//		
//		
//		for(int i=0;i<corMat.length;i++)
//		{
//			String name = train.attribute(i).name();
//			outCorMat.write(name+",");
//			
//			for(int j=0;j<corMat[0].length;j++)
//			{
//				if(j != corMat[0].length-1)
//					outCorMat.write(corMat[i][j]+",");
//				else
//					outCorMat.write(corMat[i][j]+"\n");
//			}
//		}
//		
//		outCorMat.flush();
		
//		saver.setInstances(train);
//		saver.setFile(new File("train-RegTune-CFS-GYR80.csv"));
//		saver.writeBatch();
//	
//		saver.setInstances(test);
//		saver.setFile(new File("test-RegTune-CFS-GYR80.csv"));
//		saver.writeBatch();	
//		
//		saver.setInstances(train);
//		saver.setFile(new File("train-RegTune-Const-GYR80.csv"));
//		saver.writeBatch();
//	
//		saver.setInstances(test);
//		saver.setFile(new File("test-RegTune-Const-GYR80.csv"));
//		saver.writeBatch();	
//		
//========================================= Building model
		Logistic log = new Logistic();
//		double lambda = 0.19; //Cfs SE
//		double lambda = 1.34; //Const SE
		double lambda = 0.1; //all
	
//		log.setRidge(lambda);
		
			
		Evaluation evalTest = new Evaluation(train);
		Evaluation evalTrain = new Evaluation(train);
		Evaluation evalAll = new Evaluation(train);
			
		log.buildClassifier(train);
		
		evalTest.evaluateModel(log, test);
		evalTrain.evaluateModel(log, train);
		evalAll.evaluateModel(log, newDataFull);
		
		double[][] coef = log.coefficients();
		
		String outputFile ="Coef-"+lambda+"-GYR80.csv" ;
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outputFile)));
		//out.write("Attribute,"+firstClass+","+secondClass+"\n");
		
		
		
									
		System.out.println("===================================TEST SET===========================================");
		System.out.println("Correct % = "+evalTest.pctCorrect());
		
		System.out.println("FMeasure "+firstClass+" = "+evalTest.fMeasure(0));
		System.out.println("FMeasure "+secondClass+" = "+evalTest.fMeasure(1));
		System.out.println("FMeasure "+thirdClass+" = "+evalTest.fMeasure(2));
		System.out.println("Mean FMeasure = "+(evalTest.fMeasure(0)+evalTest.fMeasure(1)+evalTest.fMeasure(2))/3.0);
		System.out.println(evalTest.toMatrixString());
		
		System.out.println("===================================ALL SET===========================================");
		System.out.println("Correct % = "+evalAll.pctCorrect());
		
		System.out.println("FMeasure "+firstClass+" = "+evalAll.fMeasure(0));
		System.out.println("FMeasure "+secondClass+" = "+evalAll.fMeasure(1));
		System.out.println("FMeasure "+thirdClass+" = "+evalAll.fMeasure(2));
		System.out.println("Mean FMeasure = "+(evalAll.fMeasure(0)+evalAll.fMeasure(1)+evalAll.fMeasure(2))/3.0);
		System.out.println(evalAll.toMatrixString());
		
		System.out.println("===================================TRAIN SET===========================================");
		System.out.println("Correct % = "+evalTrain.pctCorrect());
		
		System.out.println("FMeasure "+firstClass+" = "+evalTrain.fMeasure(0));
		System.out.println("FMeasure "+secondClass+" = "+evalTrain.fMeasure(1));
		System.out.println("FMeasure "+thirdClass+" = "+evalTrain.fMeasure(2));
		System.out.println("Mean FMeasure = "+(evalTrain.fMeasure(0)+evalTrain.fMeasure(1)+evalTrain.fMeasure(2))/3.0);
		System.out.println(evalTrain.toMatrixString());
		
		
		System.out.println("===================================COEFFICIENT===========================================");
		System.out.println(firstClass+"                                    "+secondClass);
		
		
		
		for(int i=0;i<coef.length;i++)
		{
			for(int j=0;j<coef[0].length;j++)
			{
				System.out.print(coef[i][j]+"                       ");
				
				if(j%2==0)
					out.write(i+","+coef[i][j]+",");
				else
					out.write(coef[i][j]+"\n");
			}
			
			System.out.println();
		}
		
		out.flush();
		
		System.out.println(train.numAttributes());
		System.out.println(newDataFull.numInstances());
		System.out.println(train.attribute(7).name()+",");//nama atribut dihitung mulai dari 0. kecuali pada Remove
		
		double[] att1 = train.attributeToDoubleArray(0);
		System.out.println(att1[0]);
	
	}

}

