package CrossValidationExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;

public class RegularizationTuningCV 
{
	public static void main(String[] args) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
							
		//name of the input files
		
//		String fileTrain = "train-RegTune-ConstYGR.csv";	
//		String fileTrain = "trainRegTuneCfsYGR.csv";	
//		

//		String fileTrain = "train-RegTune-Const-GYR80.csv";	
//		String fileTrain = "train-RegTune-Cfs-GYR80.csv";
		String fileTrain = "train-RegTune-All-GYR80.csv";

		
//		String fileTrain = "trainRegTuneCfsRYG.csv";	
		
//		String fileTrain = "trainRegTuneConstRYG.csv";	
		
		//load data
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[2];
		options[0]="-L";
		//options[1]="last:Yellow,Green,Red";
		options[1]="last:Green,Yellow,Red";
		//options[1]="last:Red,Yellow,Green";
		loader.setOptions(options);
		
		loader.setSource(new File(path+fileTrain));
		Instances train = loader.getDataSet();
		train.setClassIndex(train.numAttributes()-1);
		
		
		CSVSaver saver = new CSVSaver();
//		
		String outputFile = "RegTune-All-GYR80.csv";
//		String outputFile = "RegTune-Const-GYR80.csv";		
//		String outputFile = "RegTune-Cfs-GYR80.csv";


		
//		String outputFile = "RegTune2-Cv-YGRConst.csv";
//		String outputMatrix = "RegTune2-Cv-YGRConst-Matrix.txt";
		
//		String outputFile = "RegTune2-Test-YGRCfs.csv";
//		String outputMatrix = "RegTune2-Test-YGRCfs-Matrix.txt";
		
//		String outputFile = "RegTune2-Cv-RYGConst.csv";
//		String outputMatrix = "RegTune2-Cv-RYGConst-Matrix.txt";
		
//		String outputFile = "RegTune2-Test-RYGConst.csv";
//		String outputMatrix = "RegTune2-Test-RYGConst-Matrix.txt";
		
//		String outputFile = "RegTune2-Cv-RYGCfs.csv";
//		String outputMatrix = "RegTune2-Cv-RYGCfs-Matrix.txt";
		
//		String outputFile = "RegTune2-Test-RYGCfs.csv";
//		String outputMatrix = "RegTune2-Test-RYGCfs-Matrix.txt";
	
		String outputMat = "RegTune-All-GYR80.txt";
		
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outputFile)));
		BufferedWriter outMat = new BufferedWriter(new FileWriter(new File(outputMat)));
		
		out.write("Lambda,Accuracy(%),F"+train.classAttribute().value(0)+",F"+train.classAttribute().value(1)+",F"+train.classAttribute().value(2)+",FMean\n");
		
		double accBefore=-1.0;
		double f1Before=-1.0;
		double f2Before=-1.0;
		double f3Before=-1.0;
		double fMeanBefore=-1.0;
		
		for(int i=0;i<=1000;i++)
		{
			//double lambda = 0.004;
			double lambda = i*0.01;
			
			Logistic log = new Logistic();
			log.setRidge(lambda);			
			
			//randoming data
			int folds = 10;
						
			//stratify
			if(train.classAttribute().isNominal())
				train.stratify(folds);
			
			double meanAcc = 0;
			double meanF1 = 0;
			double meanF2 = 0;
			double meanF3 = 0;
			double meanFMean =0;
			
			Evaluation eval10 = new Evaluation(train);
			for(int n=0; n<folds; n++)
			{
				Instances trainCV = train.trainCV(folds, n);
				Instances testCV = train.testCV(folds, n);
				
				log.buildClassifier(trainCV);
				
				Evaluation eval = new Evaluation(train);
								
				eval.evaluateModel(log, testCV);
				eval10.evaluateModel(log,testCV);
								
				meanAcc+=eval.pctCorrect();
				meanF1 += eval.fMeasure(0);
				meanF2 += eval.fMeasure(1);
				meanF3 += eval.fMeasure(2);
				meanFMean += ((eval.fMeasure(0)+eval.fMeasure(1)+eval.fMeasure(2))/3.0);
			}
			
			meanAcc=meanAcc/folds;
			meanF1=meanF1/folds;
			meanF2=meanF2/folds;
			meanF3=meanF3/folds;
			meanFMean=meanFMean/folds;
								
			
			if(!(accBefore==meanAcc && f1Before==meanF1 && f2Before==meanF2 && f3Before==meanF3 ))
			{	
				out.write(lambda+",");
				out.write(meanAcc+",");
				out.write(meanF1+",");
				out.write(meanF2+",");
				out.write(meanF3+",");
				out.write(meanFMean+"\n");
				
				outMat.write(eval10.toMatrixString("Lambda = "+lambda));
				outMat.write("\n");
//							
//				System.out.println("===================================== regularization parameter = "+lambda+"=====================================\n");
//				System.out.println("Correct % = "+meanAcc+"\n");
//						
//				System.out.println("FMeasure = "+meanF1+"\n");
//				System.out.println("FMeasure = "+meanF2+"\n");
//				System.out.println("FMeasure = "+meanF3+"\n");
//				System.out.println("Mean FMeasure = "+meanFMean+"\n");
//				System.out.println("=================================================================================================================\n");
			}
			
			accBefore=meanAcc;
			f1Before=meanF1;
			f2Before=meanF2;
			f3Before=meanF3;
			fMeanBefore=meanFMean;
			
		}
						
		out.flush();
		outMat.flush();
	}
}
