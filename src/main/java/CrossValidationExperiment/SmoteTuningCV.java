package CrossValidationExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.unsupervised.instance.Randomize;

public class SmoteTuningCV 
{
	public static void main(String args[]) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
							
		//name of the input files
		String fileTrain = "trainNoSmote-GYR80.csv";	
				
		//load data
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[2];
		options[0]="-L";
		options[1]="last:Green,Yellow,Red";
		loader.setOptions(options);
		
		loader.setSource(new File(path+fileTrain));
		Instances train = loader.getDataSet();
		train.setClassIndex(train.numAttributes()-1);
		
		int numClass= train.classAttribute().numValues();
		
		CSVSaver saver = new CSVSaver();
			
		//get the majority class
		AttributeStats stats = train.attributeStats(train.classIndex());
		int[] classMember = stats.nominalCounts;
		
		int majorClass = 0;
			
		for(int i=1;i<classMember.length;i++)
		{
			if(classMember[i]>classMember[majorClass])
			{
				majorClass=i;				
			}
		}

		String outputFile ="Smote-GYR80.csv" ;
		String outputMat = "Smote-GYR80.txt";
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outputFile)));
		BufferedWriter outMat = new BufferedWriter(new FileWriter(new File(outputMat)));
		
		out.write("NN,Accuracy(%),F"+train.classAttribute().value(0)+",F"+train.classAttribute().value(1)+",F"+train.classAttribute().value(2)+",FMean\n");
		
		//salah, mestinya yg dismote traincvnya aja
		//do SMOTE
		
		//int j = 1;
		
		//try{
		//while(j<22)
		for(int j=1;j<=20;j++)
		{
			//System.out.println(j);
			Instances newTrain = new Instances(train);
			
			SMOTE smoter = new SMOTE();
			smoter.setRandomSeed(161803398);
			smoter.setNearestNeighbors(j);
			
			for(int i=0;i<classMember.length;i++)
			{
				if(i!=majorClass)
				{
					int percentage = (classMember[majorClass]/classMember[i]-1)*100;
					smoter.setInputFormat(newTrain);
					smoter.setPercentage(percentage);
					smoter.setClassValue((i+1)+"");
					newTrain = Filter.useFilter(newTrain, smoter);
				}
			}
			
//			saver.setInstances(newTrain);
//			saver.setFile(new File("Smote80GYR-NoRand.csv"));
//			saver.writeBatch();

			Randomize randomizer = new Randomize();
			randomizer.setRandomSeed(161803398);
			randomizer.setInputFormat(newTrain);
			newTrain = Filter.useFilter(newTrain, randomizer);	

			Logistic log = new Logistic();
			
			//randoming data
			int folds = 10;
						
			//stratify
			if(newTrain.classAttribute().isNominal())
				newTrain.stratify(folds);
			
			double meanAcc = 0;
			double meanF1 = 0;
			double meanF2 = 0;
			double meanF3 = 0;
			double meanFMean =0;
						
			Evaluation eval10=new Evaluation(newTrain);
			
			for(int n=0; n<folds; n++)
			{
				Instances trainCV = newTrain.trainCV(folds, n);
				Instances testCV = newTrain.testCV(folds, n);
				
				log.buildClassifier(trainCV);
				
				Evaluation eval = new Evaluation(newTrain);
				
				eval.evaluateModel(log, testCV);
				eval10.evaluateModel(log, testCV);
								
				meanAcc+=eval.pctCorrect();
				meanF1 += eval.fMeasure(0);
				meanF2 += eval.fMeasure(1);
				meanF3 += eval.fMeasure(2);
				meanFMean += ((eval.fMeasure(0)+eval.fMeasure(1)+eval.fMeasure(2))/3.0);

			}
			
			meanAcc=meanAcc/folds;
			meanF1=meanF1/folds;
			meanF2=meanF2/folds;
			meanF3=meanF3/folds;
			meanFMean=meanFMean/folds;
			
			out.write(j+","+meanAcc+","+meanF1+","+meanF2+","+meanF3+","+meanFMean+"\n");						
			outMat.write(eval10.toMatrixString("NN = "+j));
			outMat.write("\n");
			
		
		//	j++;
			
		}
		
//		}
//		catch(Exception e)
//		{
//			System.out.println("out");
//		}

		out.flush();
		outMat.flush();
	}	
}
