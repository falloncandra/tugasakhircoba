import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.supervised.instance.Resample;

public class DataSplitter 
{
	private Instances dataFull;
	private Instances trainSet;
	private Instances cvSet;
	private Instances testSet;
	
	public DataSplitter(Instances data)
	{
		this.dataFull = data;
	}
	
	public void splitData() throws Exception
	{
		int seed = 161803398;
		//int seed = (int) System.currentTimeMillis();
		
		//First sampler (without inversion)
		Resample sampler = new Resample();
		sampler.setRandomSeed(seed);
		sampler.setBiasToUniformClass(0);
		sampler.setNoReplacement(true);
		sampler.setInvertSelection(false);
		sampler.setSampleSizePercent(60);
		sampler.setInputFormat(dataFull);
		
		//get the train set
		Instances train = Filter.useFilter(dataFull, sampler);
		
		//Second sampler (for inversion)
		Resample sampler2 = new Resample();
		sampler2.setRandomSeed(seed);
		sampler2.setBiasToUniformClass(0);
		sampler2.setNoReplacement(true);
		sampler2.setInvertSelection(true);
		sampler2.setSampleSizePercent(60);
		sampler2.setInputFormat(dataFull);
		
		//get the cv set + test set
		Instances cvTest = Filter.useFilter(dataFull,sampler2);
		
		//get the cv set
		sampler.setSampleSizePercent(50);
		sampler.setInputFormat(cvTest);
		
		Instances test = Filter.useFilter(cvTest, sampler);
	
		//get the test set
		sampler2.setSampleSizePercent(50);
		sampler2.setInputFormat(cvTest);
		
		Instances cv = Filter.useFilter(cvTest, sampler2);
		
		this.trainSet  = train;
//		this.cvSet = test;
//		this.testSet = cv;
		
		this.cvSet = cv;
		this.testSet = test;
	}
	
	public Instances getTrainSet()
	{
		return this.trainSet;
	}
	
	public Instances getCvSet()
	{
		return this.cvSet;
	}
	
	public Instances getTestSet()
	{
		return this.testSet;
	}
}
