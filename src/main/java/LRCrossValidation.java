import java.io.File;
import java.io.IOException;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.CSVSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;



public class LRCrossValidation
{
	public static void main(String[] args) throws Exception
	{
		//get the path of this code
		String relPath = new File("").getAbsolutePath();
		relPath = relPath + "\\";
		System.out.println(relPath);
					
		//name of the input files
		String fileInput = "data-student-learning-analytics-plus-class.csv";
		
		
		CSVLoader loader = new CSVLoader();
		
		//load complete data
		loader.setSource(new File(relPath+fileInput));
		Instances data = loader.getDataSet();
			
		Remove remover = new Remove();
		//indeks atribut mulai dari 1
		remover.setAttributeIndices("1,2,3,6,7,8,9");
		remover.setInvertSelection(false);
		
		remover.setInputFormat(data);
		Instances newData = Filter.useFilter(data, remover);
		newData.setClassIndex(newData.numAttributes()-1);
					
		Logistic log = new Logistic();
		
		//randoming data
		int seed = 161803398;
		int folds = 10;
		Random rand = new Random(seed);
		
		Instances randData = new Instances(newData);
		randData.randomize(rand);
		
		//stratify
		if(randData.classAttribute().isNominal())
			randData.stratify(folds);
		
		int meanPrecision = 0;
		
		for(int n=0; n<folds; n++)
		{
			Evaluation eval = new Evaluation(randData);
			
			Instances train = randData.trainCV(folds, n);
			Instances test = randData.testCV(folds, n);
			
			log.buildClassifier(train);
			eval.evaluateModel(log, test);
			
			System.out.println("Output evaluation");
			System.out.println("Correct % = "+eval.pctCorrect());
			System.out.println("Incorrect % = "+eval.pctIncorrect());
			System.out.println("AUC = "+eval.areaUnderROC(1));
			System.out.println("kappa = "+eval.kappa());
			System.out.println("MAE = "+eval.meanAbsoluteError());
			System.out.println("RMSE = "+eval.rootMeanSquaredError());
			System.out.println("RAE = "+eval.relativeAbsoluteError());
			System.out.println("RRSE = "+eval.rootRelativeSquaredError());
			System.out.println("Precision = "+eval.precision(1));
			System.out.println("Recall = "+eval.recall(1));
			System.out.println("FMeasure = "+eval.fMeasure(1));
			System.out.println("Error Rate = "+eval.errorRate());
			System.out.println(eval.toMatrixString());
			
			meanPrecision+=eval.pctCorrect();
		}
		
		System.out.println("Mean Precision % = "+(meanPrecision/(folds*1.0)));
		
	}
		
}
