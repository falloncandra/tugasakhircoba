import java.io.File;

import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.ConsistencySubsetEval;
import weka.attributeSelection.ExhaustiveSearch;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.unsupervised.attribute.Normalize;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.Standardize;
import weka.filters.unsupervised.instance.Randomize;

public class ModelTester 
{
	
	public static void main(String[] args) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
									
		//name of the input files
		String fileModel = "LogRegYGRConst.model";	
		
		Logistic log = (Logistic) SerializationHelper.read(fileModel);
		
	
		//name of the input files
		String fileCv = "YellowGreenRed\\cvAttSelected.csv";
		String fileTest = "YellowGreenRed\\testAttSelected.csv";
		
		//load data
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[2];
		options[0]="-L";
		options[1]="last:Yellow,Green,Red";
		loader.setOptions(options);
		
		loader.setSource(new File(path+fileCv));
		Instances cv = loader.getDataSet();
		cv.setClassIndex(cv.numAttributes()-1);
		
		loader.setSource(new File(path+fileTest));
		Instances test = loader.getDataSet();
		test.setClassIndex(test.numAttributes()-1);
		
//		Evaluation evalCv = new Evaluation(train);
//		evalCv.evaluateModel(log, cv);
//		System.out.println("Correct % = "+evalCv.pctCorrect());
//				
//		System.out.println("FMeasure = "+evalCv.fMeasure(0));
//		System.out.println("FMeasure = "+evalCv.fMeasure(1));
//		System.out.println("FMeasure = "+evalCv.fMeasure(2));
//		System.out.println("Mean FMeasure = "+(evalCv.fMeasure(0)+evalCv.fMeasure(1)+evalCv.fMeasure(2))/3.0);
//		System.out.println(evalCv.toMatrixString());
//	
//		Evaluation evalTest = new Evaluation(train);
//		evalTest.evaluateModel(log, test);
//		System.out.println("Correct % = "+evalTest.pctCorrect());
//					
//		System.out.println("FMeasure = "+evalTest.fMeasure(0));
//		System.out.println("FMeasure = "+evalTest.fMeasure(1));
//		System.out.println("FMeasure = "+evalTest.fMeasure(2));
//		System.out.println("Mean FMeasure = "+(evalTest.fMeasure(0)+evalTest.fMeasure(1)+evalTest.fMeasure(2))/3.0);
//		System.out.println(evalTest.toMatrixString());
	}
//	
}
