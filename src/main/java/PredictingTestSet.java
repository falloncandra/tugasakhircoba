import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import weka.classifiers.functions.Logistic;
import weka.core.AttributeStats;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;

public class PredictingTestSet
{
	
	public static void main(String[] args) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
									
		//name of the input files
		
//		String fileModel1 = "LogRegYGRConst-cv.model";
//		String fileModel2 = "LogRegYGRConst-test.model";
		//String fileModel = "LogRegYGRCfs.model";
		
		String fileModel1 = "LogRegRYGConst-cv.model";
		String fileModel2 = "LogRegRYGConst-test.model";
		//String fileModel = "LogRegRYGCfs.model";
		
		//String fileModel = "LogRegGYRConst.model";
		//String fileModel = "LogRegGYRCfs.model";
		
		Logistic logReg1 = (Logistic) SerializationHelper.read(fileModel1);
		Logistic logReg2 = (Logistic) SerializationHelper.read(fileModel2);
		
	
		//name of the input files
		String fileTest = "dataFullRYGConst.csv";
		//String fileTest = "dataFullRYGCfs.csv";
		//String fileTest = "dataFullYGRConst.csv";
		//String fileTest = "dataFullYGRCfs.csv";
		//String fileTest = "dataFullGYRConst.csv";
		//String fileTest = "dataFullGYRCfs.csv";
		
		//load data
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[2];
		options[0]="-L";
		options[1]="last:Red,Yellow,Green";
		//options[1]="last:Yellow,Green,Red";
		//options[1]="last:Green,Yellow,Red";
		loader.setOptions(options);
			
		loader.setSource(new File(path+fileTest));
		Instances test = loader.getDataSet();
		test.setClassIndex(test.numAttributes()-1);
		
		
		//String outputFile = "dataFullYGRConst-Cv-vs-Test.txt";
		String outputFile = "dataFullRYGConst-Cv-vs-Test.txt";
		
		
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outputFile)));
		out.write("modelCV modelTest\n");
		
		for(int i=0;i<test.numInstances();i++)
		{
//			double actualClass = test.instance(i).classValue();
//			String actual = test.classAttribute().value((int)actualClass);
			
			Instance instPredict = test.instance(i);
			
			double prediction1 = logReg1.classifyInstance(instPredict);
			String predString1 = test.classAttribute().value((int)prediction1);
			
			double prediction2 = logReg2.classifyInstance(instPredict);
			String predString2 = test.classAttribute().value((int)prediction2);
			
//			out.write(actual+" "+predString+"\n");
			out.write(predString1+" "+predString2+"\n");
		}
		
		out.flush();

	}	
}

