import java.io.File;

import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.ConsistencySubsetEval;
import weka.attributeSelection.ExhaustiveSearch;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.unsupervised.attribute.Normalize;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.Standardize;
import weka.filters.unsupervised.instance.Randomize;

public class PreprocessCoba
{
	public static void main(String[] args) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
							
	
		//load data
		CSVLoader loader = new CSVLoader();
		
		loader.setSource(new File("train-nosmote.csv"));
		Instances train = loader.getDataSet();
		train.setClassIndex(train.numAttributes()-1);
		
		loader.setSource(new File("cvSet-nosmote.csv"));
		Instances cv = loader.getDataSet();
		cv.setClassIndex(cv.numAttributes()-1);
		
		loader.setSource(new File("testSet-nosmote.csv"));
		Instances test = loader.getDataSet();
		test.setClassIndex(test.numAttributes()-1);
		
		CSVSaver saver = new CSVSaver();
		
				
//		
		//get the majority class
		AttributeStats stats = train.attributeStats(train.classIndex());
		int[] classMember = stats.nominalCounts;
		
		int majorClass = 0;
			
		for(int i=1;i<classMember.length;i++)
		{
			if(classMember[i]>classMember[majorClass])
			{
				majorClass=i;				
			}
		}

//do SMOTE to handle imbalanced class
		
//with smote without standardizer or normalizer: 75% and 57.41%, all : 66..% -> banyakan yg bener diklasifikasi daripada salah
//without SMOTE: 76.93% and 59.26% -> tapi banyak banget Yellow yang salah klasifikasi jadi green
		SMOTE smoter = new SMOTE();
		smoter.setRandomSeed(161803398);
		smoter.setNearestNeighbors(8); //result from parameter tuning using default LR
		
		for(int i=0;i<classMember.length;i++)
		{
			if(i!=majorClass)
			{
				int percentage = (classMember[majorClass]/classMember[i]-1)*100;
				smoter.setInputFormat(train);
				smoter.setPercentage(percentage);
				smoter.setClassValue((i+1)+"");
				train = Filter.useFilter(train, smoter);
			}
		}
		
//Randomize
		Randomize randomizer = new Randomize();
		randomizer.setRandomSeed(161803398);
		randomizer.setInputFormat(train);
		train = Filter.useFilter(train, randomizer);	

//Standardize (Z-Score)
		Standardize standardizer = new Standardize();
		standardizer.setInputFormat(train);
		train = Filter.useFilter(train, standardizer);
		cv = Filter.useFilter(cv, standardizer);
		test = Filter.useFilter(test, standardizer);
		
//		ArffSaver saverArff = new ArffSaver();
//		saverArff.setInstances(train);
//		saverArff.setFile(new File("trainSelectCoba.arff"));
//		saverArff.writeBatch();
		
//		saverArff.setInstances(test);
//		saverArff.setFile(new File(path+"testSelectCoba.arff"));
//		saverArff.writeBatch();
//		
//		saverArff.setInstances(cv);
//		saverArff.setFile(new File(path+"cvSelectCoba.arff"));
//		saverArff.writeBatch();
		
//		Normalize normalizer = new Normalize();
//		normalizer.setInputFormat(train);
//		train = Filter.useFilter(train, normalizer);
//		cv = Filter.useFilter(cv, normalizer);
//		test = Filter.useFilter(test, normalizer);

//Hasil ConsistencySubsetEval + ExhaustiveSearch = 2,3,5,7,9,10,12,13,14,19,21

//============ Attribute Selection using ConsistencySubsetEval + ExhaustiveSearch=============//
//		AttributeSelection attSelector = new AttributeSelection();
//		ConsistencySubsetEval conEval = new ConsistencySubsetEval();
//		ExhaustiveSearch exhaustSearch = new ExhaustiveSearch();
//		
//		attSelector.setEvaluator(conEval);
//		attSelector.setSearch(exhaustSearch);
//		attSelector.setInputFormat(train);

//============ Attribute Selection using ConsistencySubsetEval + ExhaustiveSearch=============//
		
//		train = Filter.useFilter(train,attSelector);
//		cv = Filter.useFilter(cv,attSelector);
//		test = Filter.useFilter(test,attSelector);
		
//		saver.setInstances(train);
//		saver.setFile(new File("trainUnSelected.csv"));
//		saver.writeBatch();
//		
//		saver.setInstances(cv);
//		saver.setFile(new File("cvUnSelected.csv"));
//		saver.writeBatch();		
//		
//		saver.setInstances(test);
//		saver.setFile(new File("testUnSelected.csv"));
//		saver.writeBatch();	
//		
//		String retainedAtt = "2,3,5,7,9,10,12,13,14,19,21,"+(train.classIndex()+1);
//		remover.setAttributeIndices(retainedAtt);
//		remover.setInvertSelection(true);
//				
//		remover.setInputFormat(train);
//		train = Filter.useFilter(train, remover);
//		train.setClassIndex(train.numAttributes()-1);
//		
//		remover.setInputFormat(cv);
//		cv = Filter.useFilter(cv, remover);
//		remover.setInputFormat(test);
//		test = Filter.useFilter(test, remover);
		
	
//		saver.setInstances(train);
//		saver.setFile(new File("trainAttSelected.csv"));
//		saver.writeBatch();
//		
//		saver.setInstances(cv);
//		saver.setFile(new File("cvAttSelected.csv"));
//		saver.writeBatch();		
//		
//		saver.setInstances(test);
//		saver.setFile(new File("testAttSelected.csv"));
//		saver.writeBatch();	
		
		
		Logistic log = new Logistic();
		log.buildClassifier(train);
		
		Evaluation evalCv = new Evaluation(train);
		evalCv.evaluateModel(log, cv);
		System.out.println("Correct % = "+evalCv.pctCorrect());
				
		System.out.println("FMeasure = "+evalCv.fMeasure(0));
		System.out.println("FMeasure = "+evalCv.fMeasure(1));
		System.out.println("FMeasure = "+evalCv.fMeasure(2));
		System.out.println("Mean FMeasure = "+(evalCv.fMeasure(0)+evalCv.fMeasure(1)+evalCv.fMeasure(2))/3.0);
		System.out.println(evalCv.toMatrixString());
	
		Evaluation evalTest = new Evaluation(train);
		evalTest.evaluateModel(log, test);
		System.out.println("Correct % = "+evalTest.pctCorrect());
					
		System.out.println("FMeasure = "+evalTest.fMeasure(0));
		System.out.println("FMeasure = "+evalTest.fMeasure(1));
		System.out.println("FMeasure = "+evalTest.fMeasure(2));
		System.out.println("Mean FMeasure = "+(evalTest.fMeasure(0)+evalTest.fMeasure(1)+evalTest.fMeasure(2))/3.0);
		System.out.println(evalTest.toMatrixString());
//		
		
//		ArffSaver saverArff = new ArffSaver();
//		saverArff.setInstances(train);
//		saverArff.setFile(new File(path+"train-nosmote.arff"));
//		saverArff.writeBatch();
//		
//		saverArff.setInstances(test);
//		saverArff.setFile(new File(path+"test-nosmote.arff"));
//		saverArff.writeBatch();
//		
//		saverArff.setInstances(cv);
//		saverArff.setFile(new File(path+"cv-nosmote.arff"));
//		saverArff.writeBatch();
	
	}
}
