import java.io.File;

import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.ConsistencySubsetEval;
import weka.attributeSelection.ExhaustiveSearch;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.unsupervised.attribute.Normalize;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.Standardize;
import weka.filters.unsupervised.instance.Randomize;


//Green,Yellow,Red
public class PreprocessGYR
{
	public static void main(String[] args) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
							
		//name of the input files
		String fileInput = "data-student-learning-analytics-plus-class.csv";	
		
		//load data
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[4];
		options[0]="-N";
		options[1]="last";
		options[2]="-L";
		options[3]="last:Green,Yellow,Red";
		loader.setOptions(options);
		
		loader.setSource(new File(path+fileInput));
		Instances dataFull = loader.getDataSet();
		dataFull.setClassIndex(dataFull.numAttributes()-1);
		
		CSVSaver saver = new CSVSaver();
		
	
		//remove unnecessary attributes (att starts from 1)
		Remove remover = new Remove();
		remover.setAttributeIndices("1,2,3,4,5,6,7,8,9,10,11");
		remover.setInvertSelection(false);
		remover.setInputFormat(dataFull);
		Instances newDataFull = Filter.useFilter(dataFull, remover);
		
	
		//split data to train, cv, and test set (60,20,20)
		DataSplitter splitter = new DataSplitter(newDataFull);
		splitter.splitData();
		
		Instances train = splitter.getTrainSet();
		Instances cv = splitter.getCvSet();
		Instances test = splitter.getTestSet();
		
		//get the majority class
		AttributeStats stats = train.attributeStats(train.classIndex());
		int[] classMember = stats.nominalCounts;
		
		int majorClass = 0;
			
		for(int i=1;i<classMember.length;i++)
		{
			if(classMember[i]>classMember[majorClass])
			{
				majorClass=i;				
			}
		}

//		
//		saver.setInstances(train);
//		saver.setFile(new File("trainNoSmoteNom.csv"));
//		saver.writeBatch();
//		
//		saver.setInstances(cv);
//		saver.setFile(new File("cvNoSmoteNom.csv"));
//		saver.writeBatch();		
//		
//		saver.setInstances(test);
//		saver.setFile(new File("testNoSmoteNom.csv"));
//		saver.writeBatch();	
//		
		
//do SMOTE to handle imbalanced class
		
		SMOTE smoter = new SMOTE();
		smoter.setRandomSeed(161803398);
		smoter.setNearestNeighbors(8); //result from parameter tuning using default LR
		
		for(int i=0;i<classMember.length;i++)
		{
			if(i!=majorClass)
			{
				int percentage = (classMember[majorClass]/classMember[i]-1)*100;
				smoter.setInputFormat(train);
				smoter.setPercentage(percentage);
				smoter.setClassValue((i+1)+"");
				train = Filter.useFilter(train, smoter);
			}
		}
	
//Randomize
		Randomize randomizer = new Randomize();
		randomizer.setRandomSeed(161803398);
		randomizer.setInputFormat(train);
		train = Filter.useFilter(train, randomizer);	

//Standardize (Z-Score)
		Standardize standardizer = new Standardize();
		standardizer.setInputFormat(train);
		train = Filter.useFilter(train, standardizer);
		cv = Filter.useFilter(cv, standardizer);
		test = Filter.useFilter(test, standardizer);
		newDataFull = Filter.useFilter(newDataFull, standardizer);
		
//		Normalize normalizer = new Normalize();
//		normalizer.setInputFormat(train);
//		train = Filter.useFilter(train, normalizer);
//		cv = Filter.useFilter(cv, normalizer);
//		test = Filter.useFilter(test, normalizer);
		
		

//============ Attribute Selection using ConsistencySubsetEval + ExhaustiveSearch=============//
//		AttributeSelection attSelector = new AttributeSelection();
//		ConsistencySubsetEval conEval = new ConsistencySubsetEval();
//		ExhaustiveSearch exhaustSearch = new ExhaustiveSearch();
//		
//		attSelector.setEvaluator(conEval);
//		attSelector.setSearch(exhaustSearch);
//		attSelector.setInputFormat(train);
	
//		train = Filter.useFilter(train,attSelector);
//		cv = Filter.useFilter(cv,attSelector);
//		test = Filter.useFilter(test,attSelector);
		
//============ Attribute Selection using ConsistencySubsetEval + ExhaustiveSearch=============//		
		
		//String retainedAtt = "2,3,5,7,9,10,12,13,14,19,21,"+(train.classIndex()+1);//consistency SE YGR
		//String retainedAtt = "1,3,5,6,8,10,11,14,15,16,17,19,22,"+(train.classIndex()+1);//consistency SE
		String retainedAtt = "1,3,5,6,7,10,12,13,14,15,16,19,20,21,22,"+(train.classIndex()+1);//cfs SE
		remover.setAttributeIndices(retainedAtt);
		remover.setInvertSelection(true);
				
		remover.setInputFormat(train);
		train = Filter.useFilter(train, remover);
		train.setClassIndex(train.numAttributes()-1);
		
		remover.setInputFormat(cv);
		cv = Filter.useFilter(cv, remover);
		
		remover.setInputFormat(test);
		test = Filter.useFilter(test, remover);
		
		remover.setInputFormat(newDataFull);
		newDataFull = Filter.useFilter(newDataFull, remover);
		
//		saver.setInstances(newDataFull);
//		//saver.setFile(new File("dataFullGYRConst.csv"));
//		saver.setFile(new File("dataFullGYRCfs.csv"));
//		saver.writeBatch();
		
		
//		saver.setInstances(train);
//		saver.setFile(new File("trainRegTuneConstGYR.csv"));
//		saver.writeBatch();
//		
//		saver.setInstances(cv);
//		saver.setFile(new File("cvRegTuneConstGYR.csv"));
//		saver.writeBatch();		
//		
//		saver.setInstances(test);
//		saver.setFile(new File("testRegTuneConstGYR.csv"));
//		saver.writeBatch();	
//				
			
//hasil langsung tune pake data smote: ridge =0 atau default
		Logistic log = new Logistic();
		log.setRidge(0.08); //Cfs SE
		//log.setRidge(8.55); //Const SE cv dan test
		log.buildClassifier(train);
		
		
		Evaluation evalCv = new Evaluation(train);
		evalCv.evaluateModel(log, cv);
		System.out.println("Correct % = "+evalCv.pctCorrect());
				
		System.out.println("FMeasure = "+evalCv.fMeasure(0));
		System.out.println("FMeasure = "+evalCv.fMeasure(1));
		System.out.println("FMeasure = "+evalCv.fMeasure(2));
		System.out.println("Mean FMeasure = "+(evalCv.fMeasure(0)+evalCv.fMeasure(1)+evalCv.fMeasure(2))/3.0);
		System.out.println(evalCv.toMatrixString());
	
		Evaluation evalTest = new Evaluation(train);
		evalTest.evaluateModel(log, test);
		//evalTest.evaluateModel(log, newDataFull);
		//evalTest.evaluateModel(log, train);
		System.out.println("Correct % = "+evalTest.pctCorrect());
					
		System.out.println("FMeasure = "+evalTest.fMeasure(0));
		System.out.println("FMeasure = "+evalTest.fMeasure(1));
		System.out.println("FMeasure = "+evalTest.fMeasure(2));
		System.out.println("Mean FMeasure = "+(evalTest.fMeasure(0)+evalTest.fMeasure(1)+evalTest.fMeasure(2))/3.0);
		System.out.println(evalTest.toMatrixString());

		//SerializationHelper.write("LogRegGYRConst.model", log);
		//SerializationHelper.write("LogRegGYRCfs.model", log);
	}
}
