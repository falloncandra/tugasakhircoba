import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.unsupervised.instance.Randomize;


//hasil : ridge = 1.5
public class RegularizationTuning 
{
	public static void main(String[] args) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
							
		//name of the input files
		
//		String fileTrain = "trainRegTuneConstYGR.csv";	
//		String fileCv = "cvRegTuneConstYGR.csv";
		//String fileCv = "testRegTuneConstYGR.csv";
		
//		String fileTrain = "trainRegTuneCfsYGR.csv";	
//		String fileCv = "cvRegTuneCfsYGR.csv";
//		String fileCv = "testRegTuneCfsYGR.csv";
		
//		String fileTrain = "trainRegTuneConstGYR.csv";
//		String fileCv = "testRegTuneConstGYR.csv";
//		String fileCv = "cvRegTuneConstGYR.csv";
		
//		String fileTrain = "trainRegTuneCfsGYR.csv";
//		String fileCv = "testRegTuneCfsGYR.csv";
//		String fileCv = "cvRegTuneCfsGYR.csv";
		
//		String fileTrain = "trainRegTuneConstRYG.csv";	
//		String fileCv = "cvRegTuneConstRYG.csv";
//		String fileCv = "testRegTuneConstRYG.csv";
		
		String fileTrain = "trainRegTuneCfsRYG.csv";	
//		String fileCv = "cvRegTuneCfsRYG.csv";
		String fileCv = "testRegTuneCfsRYG.csv";
		
		//load data
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[2];
		options[0]="-L";
		//options[1]="last:Yellow,Green,Red";
		//options[1]="last:Green,Yellow,Red";
		options[1]="last:Red,Yellow,Green";
		loader.setOptions(options);
		
		loader.setSource(new File(path+fileTrain));
		Instances train = loader.getDataSet();
		train.setClassIndex(train.numAttributes()-1);
		
		
		loader.setSource(new File(path+fileCv));
		Instances test = loader.getDataSet();
		test.setClassIndex(test.numAttributes()-1);
		
		CSVSaver saver = new CSVSaver();
		
//		String outputFile = "RegTune-Cv-GYRCfs.csv";
//		String outputMatrix = "RegTune-Cv-GYRCfs-Matrix.txt";
//		String outputFile = "RegTune-Test-GYRCfs.csv";
//		String outputMatrix = "RegTune-Test-GYRCfs-Matrix.txt";
//		String outputFile = "RegTune-Cv-GYRConst.csv";
//		String outputMatrix = "RegTune-Cv-GYRConst-Matrix.txt";
//		String outputFile = "RegTune-Test-GYRConst.csv";
//		String outputMatrix = "RegTune-Test-GYRConst-Matrix.txt";
		
//		String outputFile = "RegTune-Cv-YGRConst.csv";
//		String outputMatrix = "RegTune-Cv-YGRConst-Matrix.txt";
//		String outputFile = "RegTune-Test-YGRCfs.csv";
//		String outputMatrix = "RegTune-Test-YGRCfs-Matrix.txt";
		
		
//		String outputFile = "RegTune-Cv-RYGCfs.csv";
//		String outputMatrix = "RegTune-Cv-RYGCfs-Matrix.txt";
		String outputFile = "RegTune-Test-RYGCfs.csv";
		String outputMatrix = "RegTune-Test-RYGCfs-Matrix.txt";
		
//		String outputFile = "RegTune-Cv-RYGConst.csv";
//		String outputMatrix = "RegTune-Cv-RYGConst-Matrix.txt";
//		String outputFile = "RegTune-Test-RYGConst.csv";
//		String outputMatrix = "RegTune-Test-RYGConst-Matrix.txt";

		
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outputFile)));
		BufferedWriter outMat = new BufferedWriter(new FileWriter(new File(outputMatrix)));
		
	//	out.write("Lambda,Accuracy(%),F Yellow, F Green, F Red, Mean F\n");
	//	out.write("Lambda,Accuracy(%), F Green, F Yellow, F Red, Mean F\n");
		out.write("Lambda,Accuracy(%), F Red, F Yellow, F Green, Mean F\n");
		
		double accBefore=-1.0;
		double f0Before=-1.0;
		double f1Before=-1.0;
		double f2Before=-1.0;
		double fMeanBefore=-1.0;
		
		for(int i=0;i<=1500;i++)
		{
			//double lambda = 0.004;
			double lambda = i*0.01;
			
			Logistic log = new Logistic();
			log.setRidge(lambda);
			
			log.buildClassifier(train);
			
			Evaluation eval = new Evaluation(train);
			//eval.evaluateModel(log, cv);
			eval.evaluateModel(log, test);
			
			double accNow=eval.pctCorrect();
			double f0Now=eval.fMeasure(0);
			double f1Now=eval.fMeasure(1);
			double f2Now=eval.fMeasure(2);
			double fMeanNow=(eval.fMeasure(0)+eval.fMeasure(1)+eval.fMeasure(2))/3.0;
			
			if(!(accBefore==accNow && f0Before==f0Now && f1Before==f1Now && f2Before==f2Now ))
			{	
				out.write(lambda+",");
				out.write(eval.pctCorrect()+",");
				out.write(eval.fMeasure(0)+",");
				out.write(eval.fMeasure(1)+",");
				out.write(eval.fMeasure(2)+",");
				out.write((eval.fMeasure(0)+eval.fMeasure(1)+eval.fMeasure(2))/3.0+"\n");
				
				outMat.write("===================================== regularization parameter = "+lambda+"=====================================\n");
				outMat.write("Correct % = "+eval.pctCorrect()+"\n");
						
				outMat.write("FMeasure = "+eval.fMeasure(0)+"\n");
				outMat.write("FMeasure = "+eval.fMeasure(1)+"\n");
				outMat.write("FMeasure = "+eval.fMeasure(2)+"\n");
				outMat.write("Mean FMeasure = "+(eval.fMeasure(0)+eval.fMeasure(1)+eval.fMeasure(2))/3.0+"\n");
				outMat.write(eval.toMatrixString()+"\n");
				outMat.write("=================================================================================================================\n");
				
				System.out.println("===================================== regularization parameter = "+lambda+"=====================================");
				System.out.println("Correct % = "+eval.pctCorrect());
						
				System.out.println("FMeasure = "+eval.fMeasure(0));
				System.out.println("FMeasure = "+eval.fMeasure(1));
				System.out.println("FMeasure = "+eval.fMeasure(2));
				System.out.println("Mean FMeasure = "+(eval.fMeasure(0)+eval.fMeasure(1)+eval.fMeasure(2))/3.0);
				System.out.println(eval.toMatrixString());
				System.out.println("=================================================================================================================");
				System.out.println();
			}
			
			accBefore=accNow;
			f0Before=f0Now;
			f1Before=f1Now;
			f2Before=f2Now;
			fMeanBefore=fMeanNow;
			
		}
						
		out.flush();
		outMat.flush();
	}
}
