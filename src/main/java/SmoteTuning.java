import java.io.File;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.unsupervised.attribute.Normalize;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.Standardize;
import weka.filters.unsupervised.instance.Randomize;


//hasil : nearest neighbor = 8
public class SmoteTuning
{
	public static void main(String[] args) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
							
		//name of the input files
		String fileTrain = "trainNoSmoteYGR.csv";	
		String fileCv = "cvNoSmoteYGR.csv";	
		
		//load data
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[2];
		options[0]="-L";
		options[1]="last:Yellow,Green,Red";
		loader.setOptions(options);
		
		loader.setSource(new File(path+fileTrain));
		Instances train = loader.getDataSet();
		train.setClassIndex(train.numAttributes()-1);
		
		loader.setSource(new File(path+fileCv));
		Instances cv = loader.getDataSet();
		cv.setClassIndex(cv.numAttributes()-1);
		
		CSVSaver saver = new CSVSaver();
		
		AttributeStats stats = train.attributeStats(train.classIndex());
		int[] classMember = stats.nominalCounts;
		
		int majorClass = 0;
			
		for(int i=1;i<classMember.length;i++)
		{
			if(classMember[i]>classMember[majorClass])
			{
				majorClass=i;				
			}
		}

		
		for(int j=1;j<=15;j++)
		{
			System.out.println(j);
			Instances newTrain = new Instances(train);
			
			SMOTE smoter = new SMOTE();
			smoter.setRandomSeed(161803398);
			//smoter.setRandomSeed(234567891);
			smoter.setNearestNeighbors(j);
			
			for(int i=0;i<classMember.length;i++)
			{
				if(i!=majorClass)
				{
					int percentage = (classMember[majorClass]/classMember[i]-1)*100;
					smoter.setInputFormat(newTrain);
					smoter.setPercentage(percentage);
					smoter.setClassValue((i+1)+"");
					newTrain = Filter.useFilter(newTrain, smoter);
				}
			}
			
//			saver.setInstances(newTrain);
//			saver.setFile(new File("trainSmoteCoba.csv"));
//			saver.writeBatch();
			
			Randomize randomizer = new Randomize();
			randomizer.setRandomSeed(161803398);
			randomizer.setInputFormat(newTrain);
			newTrain = Filter.useFilter(newTrain, randomizer);	

			Logistic log = new Logistic();
			log.buildClassifier(newTrain);
			
			Evaluation evalCv = new Evaluation(newTrain);
			evalCv.evaluateModel(log, cv);
			
			System.out.println("===================================== nearest neighbours = "+j+"=====================================");
			System.out.println("Correct % = "+evalCv.pctCorrect());
					
			System.out.println("FMeasure = "+evalCv.fMeasure(0));
			System.out.println("FMeasure = "+evalCv.fMeasure(1));
			System.out.println("FMeasure = "+evalCv.fMeasure(2));
			System.out.println("Mean FMeasure = "+(evalCv.fMeasure(0)+evalCv.fMeasure(1)+evalCv.fMeasure(2))/3.0);
			System.out.println(evalCv.toMatrixString());
			System.out.println("========================================================================================");
			System.out.println();

		}
				
		
	}
}
