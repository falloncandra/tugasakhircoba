import java.io.File;
import java.util.Random;

import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.supervised.instance.Resample;

public class SplitDataSet
{
	//a program to split dataset into 3 sets: 60% train set, 20% cv set, 20% test set. 
	//class distribution in the subset follows the class distribution in the original dataset
	public static void main (String [] args) throws Exception
	{
		//get the path of this code
		String path = new File("").getAbsolutePath();
		path = path + "\\";
							
		//name of the input files
		String fileInput = "data-student-learning-analytics-plus-class-27.csv";	
		
		//load data
		CSVLoader loader = new CSVLoader();
		loader.setSource(new File(path+fileInput));
		Instances data = loader.getDataSet();
		data.setClassIndex(data.numAttributes()-1);
		
		int seed = 161803398;
		//int seed = (int) System.currentTimeMillis();
		
		//First sampler (without inversion)
		Resample sampler = new Resample();
		sampler.setRandomSeed(seed);
		sampler.setBiasToUniformClass(0);
		sampler.setNoReplacement(true);
		sampler.setInvertSelection(false);
		sampler.setSampleSizePercent(60);
		sampler.setInputFormat(data);
		
		//get the train set
		Instances trainSet = Filter.useFilter(data, sampler);
		
		//Second sampler (for inversion)
		Resample sampler2 = new Resample();
		sampler2.setRandomSeed(seed);
		sampler2.setBiasToUniformClass(0);
		sampler2.setNoReplacement(true);
		sampler2.setInvertSelection(true);
		sampler2.setSampleSizePercent(60);
		sampler2.setInputFormat(data);
		
		//get the cv set + test set
		Instances cvTestSet = Filter.useFilter(data,sampler2);
		
		//get the cv set
		sampler.setSampleSizePercent(50);
		sampler.setInputFormat(cvTestSet);
		
		Instances cvSet = Filter.useFilter(cvTestSet, sampler);
	
		//get the test set
		sampler2.setSampleSizePercent(50);
		sampler2.setInputFormat(cvTestSet);
		
		Instances testSet = Filter.useFilter(cvTestSet, sampler2);
		
		//write the train, cv, and test set to separated files
		CSVSaver saver = new CSVSaver();
		saver.setInstances(trainSet);
		saver.setFile(new File("trainSet-test.csv"));
		saver.writeBatch();
		
		saver.setInstances(cvTestSet);
		saver.setFile(new File("cvTestSet-test.csv"));
		saver.writeBatch();
		
		saver.setInstances(cvSet);
		saver.setFile(new File("cvSet-test.csv"));
		saver.writeBatch();
		
		saver.setInstances(testSet);
		saver.setFile(new File("testSet-test.csv"));
		saver.writeBatch();
	}
	
}
