package modularExperiment;

import java.io.File;

import weka.core.AttributeStats;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

public class AttributeRemover 
{
	public static void main(String[] args) throws Exception
	{
		for(int week=3;week<=18;week++)
		{
			//get the path of this program
			String path = new File("").getAbsolutePath();
			//path = path + "\\TrainSet\\";
			path = path + "\\TestSet\\";
			
			//String fileName = "feature-SDA-2013-Genap-academic-week-"+week+".csv";
			//String fileOutput = "feature-count-SDA-2013-Genap-academic-week-"+week+".csv";
			//String fileOutput = "feature-unique-SDA-2013-Genap-academic-week-"+week+".csv";
			//String fileOutput = "feature-timeWithoutMinDiffEnrol-SDA-2013-Genap-academic-week-"+week+".csv";
			
			String fileName = "feature-SDA-2014-Genap-academic-week-"+week+".csv";
			//String fileOutput = "feature-count-SDA-2014-Genap-academic-week-"+week+".csv";
			//String fileOutput = "feature-unique-SDA-2014-Genap-academic-week-"+week+".csv";
			String fileOutput = "feature-timeWithoutMinDiffEnrol-SDA-2014-Genap-academic-week-"+week+".csv";
			
			CSVLoader loader = new CSVLoader();
			
			String[] options = new String[4];
			options[0]="-N";
			options[1]="last";
			options[2]="-L";
			options[3]="last:Green,Yellow,Red";
			loader.setOptions(options);
			
			loader.setSource(new File(path+fileName));
			Instances originData = loader.getDataSet();
			originData.setClassIndex(originData.numAttributes()-1);
			
			Remove remover = new Remove();
			
			//fitur count only
//			remover.setAttributeIndices("1,18,19,20,21,22,23,24,25,26,27,28");
//			remover.setInvertSelection(false);
			
			//fitur unique only
//			remover.setAttributeIndices("20,21,22,23,24,29");
//			remover.setInvertSelection(true);
//			
//			//fitur time without min-diff-enrol-all-course
			remover.setAttributeIndices("18,19,25,26,27,28,29");
			remover.setInvertSelection(true);
			
			remover.setInputFormat(originData);
			Instances dataResult = Filter.useFilter(originData, remover);
			
			CSVSaver saver = new CSVSaver();
			saver.setInstances(dataResult);
			saver.setFile(new File(fileOutput));
			saver.writeBatch();
		}
	}
}
