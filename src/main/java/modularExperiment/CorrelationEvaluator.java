package modularExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

//to evaluate correlation between features and class 
public class CorrelationEvaluator 
{
	public static void main(String [] args) throws Exception
	{
		String path = new File("").getAbsolutePath();
		path = path+ File.separator;
							
		//name of the folder used in this experiment
		String folderInput = "Korelasi";
		String folderExpResultName = "ExperimentResult"+ File.separator+folderInput;
		
		//Creating folder for experiment result
		File folderExpResult =  new File(folderExpResultName);
		if(!folderExpResult.exists())
			folderExpResult.mkdirs();
		
		String fileOutput = "correlation-result-SDA-2013-Genap-week-All.csv";
		
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(folderExpResultName+ File.separator +fileOutput)));
		
		
		for(int week = 3; week<=18;week++)
		{
			String fileInput = "correlation-SDA-2013-Genap-week-"+week+".csv";	
							
			CSVLoader loader = new CSVLoader();
			
			loader.setSource(new File(path+folderInput+ File.separator +fileInput));
			Instances originData = loader.getDataSet();
			
			Remove remover = new Remove();
			remover.setAttributeIndices("1");
			remover.setInvertSelection(false);
			
			remover.setInputFormat(originData);
			Instances dataset = Filter.useFilter(originData, remover);
			
			int numAtt = dataset.numAttributes();
			int numInst = dataset.numInstances();
			int classAtt = dataset.numAttributes()-1;
			
			if(week==3)
			{
				StringBuilder str = new StringBuilder();
				str.append("week,");
				
				for(int i=0;i<numAtt;i++)
				{
					if(i!=classAtt)
						str.append(dataset.attribute(i).name()+",");
					else
						str.append(dataset.attribute(i).name()+"\n");
				}				
				out.write(str.toString());
			}
				
	//		CSVSaver saver = new CSVSaver();
	//		saver.setInstances(train);
	//		saver.setFile(new File("correlation.csv"));
	//		saver.writeBatch();
			
			StringBuilder line = new StringBuilder();
			line.append(week+",");
			
			for(int i=0;i<classAtt;i++)
			{
				
				double[] att1 = dataset.attributeToDoubleArray(i);
				double[] att2 = dataset.attributeToDoubleArray(classAtt);
				
				double corr = Utils.correlation(att1, att2, numInst);
				
				if(i!=classAtt-1)
					line.append(corr+",");
				else
					line.append(corr+"\n");
			}
			
			out.write(line.toString());
		}
			
		out.close();
		
	}
}
