package modularExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import weka.core.Instances;
import weka.core.Utils;

public class CorrelationMatrix
{
	
	private Instances dataset;
	private double [][] corMatrix;
	
	public CorrelationMatrix(Instances dataset)
	{
		this.dataset =  dataset;
	}
	
	public void generateCorrelationMatrix()
	{
		int numAtt = dataset.numAttributes()-1;
		int numInst = dataset.numInstances();
		
		double[][] corMat = new double[numAtt][numAtt];
		
		for(int i=0;i<numAtt;i++)
		{
			for(int j=0;j<numAtt;j++)
			{
				if(i != j)
				{
					double[] att1 = dataset.attributeToDoubleArray(i);
					double[] att2 = dataset.attributeToDoubleArray(j);
					
					double corr = Utils.correlation(att1, att2, numInst);
					corMat[i][j] = corr;
				}
				else
					corMat[i][j]=100;
			}
		}
		
		this.corMatrix = corMat; 
	}
	
	public double[][] getCorrelationMatrix()
	{
		return corMatrix;
	}
	
	public void printCorrelationMatrix(String fileName) throws IOException
	{
		BufferedWriter outCorMat = new BufferedWriter(new FileWriter(new File(fileName)));
		
		//print header
		outCorMat.write("AttName,");
		for(int i=0;i<corMatrix.length;i++)
		{
			if(i != corMatrix[0].length-1)
				outCorMat.write(dataset.attribute(i).name()+",");
			else
				outCorMat.write(dataset.attribute(i).name()+"\n");
		}
		
		
		for(int i=0;i<corMatrix.length;i++)
		{
			String name = dataset.attribute(i).name();
			outCorMat.write(name+",");
			
			for(int j=0;j<corMatrix[0].length;j++)
			{
				if(j != corMatrix[0].length-1)
					outCorMat.write(corMatrix[i][j]+",");
				else
					outCorMat.write(corMatrix[i][j]+"\n");
			}
		}
		
		outCorMat.flush();
		outCorMat.close();
	}
}
