package modularExperiment;

import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.supervised.instance.Resample;

//Class to split dataset into 80% train set and 20% test set
public class DataSplitter 
{
	private Instances dataFull;
	private Instances trainSet;
	private Instances testSet;
		
	public DataSplitter(Instances data)
	{
		this.dataFull = data;
	}
	
	public void splitData() throws Exception
	{
		int seed = 161803398;
			
		//First sampler (without inversion)
		Resample sampler = new Resample();
		sampler.setRandomSeed(seed);
		sampler.setBiasToUniformClass(0);
		sampler.setNoReplacement(true);
		sampler.setInvertSelection(false);
		sampler.setSampleSizePercent(80);
		sampler.setInputFormat(dataFull);
		
		//get the train set
		Instances train = Filter.useFilter(dataFull, sampler);
		
		//Second sampler (for inversion)
		Resample sampler2 = new Resample();
		sampler2.setRandomSeed(seed);
		sampler2.setBiasToUniformClass(0);
		sampler2.setNoReplacement(true);
		sampler2.setInvertSelection(true);
		sampler2.setSampleSizePercent(80);
		sampler2.setInputFormat(dataFull);
		
		//get the test set
		Instances test = Filter.useFilter(dataFull,sampler2);
				
		this.trainSet  = train;
		this.testSet = test;
	}
	
	public Instances getTrainSet()
	{
		return this.trainSet;
	}
		
	public Instances getTestSet()
	{
		return this.testSet;
	}
}

