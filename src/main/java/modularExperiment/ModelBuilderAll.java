package modularExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import weka.classifiers.functions.Logistic;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.Standardize;


public class ModelBuilderAll
{
	public static void main (String args[]) throws Exception
	{
		String features = args[0];
		
		//get the path of this program
		String path = new File("").getAbsolutePath();
		path = path+ File.separator;
							
		//name of the folder used in this experiment
		String folderTest = "TestSet";
		String folderTrain = "TrainSet";
		String folderExpResultName = "ExperimentResult"+ File.separator+"Week-All"+ File.separator+features;
		
		//Creating folder for experiment result
		File folderExpResult =  new File(folderExpResultName);
		if(!folderExpResult.exists())
			folderExpResult.mkdirs();
		
		String outFile = folderExpResult + File.separator + "Evaluation-"+ features+"-Week-All.csv";
		String outMatrix = folderExpResult + File.separator + "EvaluationMatrix-"+ features+"-Week-All.txt";
		String fileChart = folderExpResult + File.separator + "EvalChart-"+ features+"-Week-All.csv";
		String fileSMOTE = folderExpResult + File.separator + "SMOTEResult-"+ features+"-Week-All.csv";
		String fileLambda = folderExpResult + File.separator + "LambdaResult-"+ features+"-Week-All.csv";
		
				
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outFile)));
		out.write("Week,Accuracy(%),F Green,F Yellow,F Red,F Mean\n");
		BufferedWriter outMat  = new BufferedWriter(new FileWriter(new File(outMatrix)));
		
		BufferedWriter outChart = new BufferedWriter(new FileWriter(new File(fileChart)));
		outChart.write("Week,Accuracy,F Green,F Yellow,F Red,Mean F\n");
		
		BufferedWriter outSMOTE = new BufferedWriter(new FileWriter(new File(fileSMOTE)));
		outSMOTE.write("Week,S\n");
		BufferedWriter outLambda = new BufferedWriter(new FileWriter(new File(fileLambda)));
		outLambda.write("Week,Lambda\n");
		
		ModelEvaluatorAll evaluator = new ModelEvaluatorAll(out, outMat, outChart);
				
		for(int week = 3; week <=18; week++)
		{
			String info = "TestSet-"+features+"-Week-"+week;
						
			//name of the dataset file	
			//feature jumlah only
			String fileTrain = "feature-"+features+"-SDA-2013-Genap-week-"+week+".csv";
			String fileTest = "feature-"+features+"-SDA-2014-Genap-week-"+week+".csv";
						
			//load data
			CSVLoader loader = new CSVLoader();
			
			String[] options = new String[4];
			options[0]="-N";
			options[1]="last";
			options[2]="-L";
			options[3]="last:Green,Yellow,Red";
			loader.setOptions(options);
			
			loader.setSource(new File(path+folderTrain+ File.separator +fileTrain));
			Instances originTrain = loader.getDataSet();
			originTrain.setClassIndex(originTrain.numAttributes()-1);
			
			loader.setSource(new File(path+folderTest+ File.separator +fileTest));
			Instances originTest = loader.getDataSet();
			originTest.setClassIndex(originTest.numAttributes()-1);
			
			//untuk print koefisien model per minggu
			String fileCoef = folderExpResult + File.separator+ "Coefficient-"+info+".csv";
			String fileModel = folderExpResult + File.separator+ "ModelInfo-"+info+".txt";
			
			BufferedWriter outCoef = new BufferedWriter(new FileWriter(new File(fileCoef)));
			outCoef.write("Attribute,"+originTest.classAttribute().value(0)+","+originTest.classAttribute().value(1)+"\n");
			BufferedWriter outModel  = new BufferedWriter(new FileWriter(new File(fileModel)));
								
			//remove unnecessary attributes (att starts from 1)
			Remove remover = new Remove();
			remover.setAttributeIndices("1");
			remover.setInvertSelection(false);
			
			remover.setInputFormat(originTrain);
			Instances train = Filter.useFilter(originTrain, remover);
			remover.setInputFormat(originTest);
			Instances test = Filter.useFilter(originTest, remover);
				
	//		String firstClass = train.classAttribute().value(0);
	//		String secondClass = train.classAttribute().value(1);
	//		String thirdClass = train.classAttribute().value(2);
			
	//========================================= Print Correlation Matrix
	/*		String fileCorMat = "CorrelationMatrix.csv";
			
			CorrelationMatrix corMatGen = new CorrelationMatrix(train);
			corMatGen.generateCorrelationMatrix();
			corMatGen.printCorrelationMatrix(folderExpResult+ File.separator +fileCorMat);*/
			
	//========================================= SMOTE Tuning and Execution
			SMOTETuner SmoteTuner = new SMOTETuner(train);
			int bestS = SmoteTuner.tuneParamS(folderExpResultName, info);
			System.out.println(bestS);
			outSMOTE.write(week+","+bestS+"\n");
					
			SMOTEExecutor SmoteExe = new SMOTEExecutor(train);
			Instances smotedTrain = SmoteExe.applySMOTE(bestS);
			
	//========================================= Standardize (Z-Score)
			Standardize standardizer = new Standardize();
			standardizer.setInputFormat(smotedTrain);
			Instances standardTrain = Filter.useFilter(smotedTrain, standardizer);
			Instances standardTest = Filter.useFilter(test, standardizer);
						
	//========================================= Regularization Parameter Tuning and Execution
			RegularizationTuner RegTuner = new RegularizationTuner(standardTrain);
			double lambda = RegTuner.tuneParamLambda(folderExpResultName, info);
			System.out.println(lambda);
			outLambda.write(week+","+lambda+"\n");
					
	//========================================= Building model		
			Logistic log = new Logistic();
			log.setRidge(lambda);
			log.buildClassifier(standardTrain);
				
			String modelName = folderExpResultName+File.separator+"RMLogReg-"+info+".model";
			SerializationHelper.write(modelName, log);
			
	//========================================== Evaluation
			evaluator.evaluate(log, standardTrain, standardTest, week, info);
			evaluator.printCoefficient(log, outCoef, outModel, standardTrain);
			outCoef.close();
			outModel.close();
		}
		
		out.close();
		outMat.close();
		outChart.close();
		outSMOTE.close();
		outLambda.close();
	}
}
