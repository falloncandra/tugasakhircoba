package modularExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import weka.classifiers.functions.Logistic;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.Standardize;


public class ModelBuilderOneSource 
{
	public static void main (String args[]) throws Exception
	{
		int week = 18;
		String info = "TestSet-Week-"+week;
		
		//get the path of this program
		String path = new File("").getAbsolutePath();
		path = path + "\\";
							
		//name of the folder used in this experiment
		String folderTrain = "TrainSet";
		String folderExpResultName = "ExperimentResult\\Week-"+week;
		
		//Creating folder for experiment result
		File folderExpResult =  new File(folderExpResultName);
		if(!folderExpResult.exists())
			folderExpResult.mkdir();
				
		//name of the dataset file
		String fileInput = "data-student-learning-analytics-plus-class.csv";	
		
		//load data
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[4];
		options[0]="-N";
		options[1]="last";
		options[2]="-L";
		options[3]="last:Green,Yellow,Red";
		loader.setOptions(options);
		
		loader.setSource(new File(path+folderTrain+"\\"+fileInput));
		Instances originData = loader.getDataSet();
		originData.setClassIndex(originData.numAttributes()-1);
							
		//remove unnecessary attributes (att starts from 1)
		Remove remover = new Remove();
		remover.setAttributeIndices("1,2,3,4,5,6,7,8,9,10,11");
		remover.setInvertSelection(false);
		remover.setInputFormat(originData);
		Instances newOriginData = Filter.useFilter(originData, remover);
		
		//split data to train, test set (80,20)
		DataSplitter splitter = new DataSplitter(newOriginData);
		splitter.splitData();
		
		Instances train = splitter.getTrainSet();
		Instances test = splitter.getTestSet();
		
//		String firstClass = train.classAttribute().value(0);
//		String secondClass = train.classAttribute().value(1);
//		String thirdClass = train.classAttribute().value(2);

//========================================= Print Correlation Matrix
/*		String fileCorMat = "CorrelationMatrix.csv";
		
		CorrelationMatrix corMatGen = new CorrelationMatrix(standardTrain);
		corMatGen.generateCorrelationMatrix();
		corMatGen.printCorrelationMatrix(folderExpResult+"\\"+fileCorMat);*/
		
//========================================= SMOTE Tuning and Execution
		SMOTETuner SmoteTuner = new SMOTETuner(train);
		int bestS = SmoteTuner.tuneParamS(folderExpResultName, info);
		System.out.println(bestS);
				
		SMOTEExecutor SmoteExe = new SMOTEExecutor(train);
		Instances smotedTrain = SmoteExe.applySMOTE(bestS);
		
//========================================= Standardize (Z-Score)
		Standardize standardizer = new Standardize();
		standardizer.setInputFormat(smotedTrain);
		Instances standardTrain = Filter.useFilter(smotedTrain, standardizer);
		
		//standardizer.setInputFormat(test); gunakan ini kalo test dan train berasal dari populasi berbeda
		Instances standardTest = Filter.useFilter(test, standardizer);
		//newOriginData = Filter.useFilter(newOriginData, standardizer);
		

//========================================= Regularization Parameter Tuning and Execution
		RegularizationTuner RegTuner = new RegularizationTuner(standardTrain);
		double lambda = RegTuner.tuneParamLambda(folderExpResultName, info);
		System.out.println(lambda);
				

//========================================= Building model		
		Logistic log = new Logistic();
		log.setRidge(lambda);
		log.buildClassifier(standardTrain);
		
		String modelName = folderExpResultName+"\\RMLogReg-"+info+".model";
		SerializationHelper.write(modelName, log);
		
//========================================== Evaluation
		String outFile = folderExpResult +"\\Evaluation-"+info+".csv";
		String outMatrix = folderExpResult +"\\EvaluationMatrix"+info+".txt";
		String fileCoef = folderExpResult +"\\Coefficient-"+info+".csv";
		String fileModel = folderExpResult +"\\ModelInfo-"+info+".txt";
				
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outFile)));
		out.write("Week,Accuracy(%),F"+standardTest.classAttribute().value(0)+",F"+standardTest.classAttribute().value(1)+",F"+standardTest.classAttribute().value(2)+",FMean\n");
		
		BufferedWriter outMat  = new BufferedWriter(new FileWriter(new File(outMatrix)));
		BufferedWriter outCoef = new BufferedWriter(new FileWriter(new File(fileCoef)));
		outCoef.write("Attribute,"+standardTest.classAttribute().value(0)+","+standardTest.classAttribute().value(1)+"\n");
		BufferedWriter outModel  = new BufferedWriter(new FileWriter(new File(fileModel)));
		
		ModelEvaluator evaluator = new ModelEvaluator(out, outMat);
				
		evaluator.evaluate(log, standardTrain, standardTest, week, info);
		evaluator.printCoefficient(log, outCoef, outModel, standardTrain);
		
		out.close();
		outMat.close();
		outCoef.close();
		outModel.close();
	}
}
