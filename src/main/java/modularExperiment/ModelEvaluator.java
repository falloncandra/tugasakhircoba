package modularExperiment;

import java.io.BufferedWriter;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;

public class ModelEvaluator
{
	private BufferedWriter out;
	private BufferedWriter outMat;
	
	public ModelEvaluator(BufferedWriter out, BufferedWriter outMat)
	{
		this.out = out;
		this.outMat = outMat;
	}
	
	public void evaluate(Logistic logReg, Instances train, Instances test, int week, String info) throws Exception
	{
		Evaluation eval = new Evaluation(train);				
		eval.evaluateModel(logReg, test);
				
		String firstClass = test.classAttribute().value(0);
		String secondClass = test.classAttribute().value(1);
		String thirdClass = test.classAttribute().value(2);
											
		System.out.println("==================================="+info+"===========================================");
		System.out.println("Correct % = "+eval.pctCorrect());
		
		System.out.println("FMeasure "+firstClass+" = "+eval.fMeasure(0));
		System.out.println("FMeasure "+secondClass+" = "+eval.fMeasure(1));
		System.out.println("FMeasure "+thirdClass+" = "+eval.fMeasure(2));
		
		double meanF = (eval.fMeasure(0)+eval.fMeasure(1)+eval.fMeasure(2))/3.0;
		System.out.println("Mean FMeasure = "+meanF);
		System.out.println(eval.toMatrixString(info));
		
		//print to file		
		out.append(week+","+eval.pctCorrect()+","+eval.fMeasure(0)+","+eval.fMeasure(1)+","+eval.fMeasure(2)+","+meanF+"\n");
		outMat.append(eval.toMatrixString(info)+"\n");
	}
	
	public void printCoefficient(Logistic logReg, BufferedWriter outCoef, BufferedWriter outModel, Instances train) throws Exception
	{
		//koefisien index 0 yang dikembalikan adalah intercept
		//atribut dihitung dari 0 kecuali pada kelas Remove
		double[][] coef = logReg.coefficients();
		
		for(int i=0;i<coef.length;i++)
		{
			if(i==0)
				outCoef.write("intercept,");
			else
				outCoef.write(train.attribute(i-1).name()+",");
			
			for(int j=0;j<coef[0].length;j++)
			{				
				if(j%2==0)
					outCoef.write(coef[i][j]+",");
				else
					outCoef.write(coef[i][j]+"\n");
			}
		}	
		
		outModel.write(logReg.toString());
	}
}
