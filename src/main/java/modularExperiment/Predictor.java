package modularExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import weka.classifiers.functions.Logistic;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.Standardize;

public class Predictor 
{
	public static void main (String[] args) throws Exception
	{
		String lacourse = args[0];
		int week = Integer.parseInt(args[1]);
		String actual = args[2];
		
		boolean hasActualClass=false;
		
		if(actual.equalsIgnoreCase("y"))
			hasActualClass = true;
			
		
		//get the path of this program
		String path = new File("").getAbsolutePath();
		path = path+ File.separator;
							
		//name of the folder used in this experiment
		String folderTrain = "Train"+File.separator;
		String folderModel = "Model"+ File.separator;
		String folderInput = "ForPrediction"+ File.separator;
		String folderPredictionName = "Prediction"+ File.separator;
		
		//Creating folder for model result
		File folderPrediction =  new File(folderPredictionName);
		if(!folderPrediction.exists())
			folderPrediction.mkdirs();
		
		//loading trainset
		String fileTrain = "train-week-"+week+".csv";
		
		CSVLoader loader = new CSVLoader();
		
		String[] options = new String[4];
		options[0]="-N";
		options[1]="last";
		options[2]="-L";
		options[3]="last:Green,Yellow,Red";
		loader.setOptions(options);
		
		loader.setSource(new File(path+folderTrain+fileTrain));
		Instances originTrain = loader.getDataSet();
		originTrain.setClassIndex(originTrain.numAttributes()-1);
													
		//remove unnecessary attributes (att starts from 1)
		Remove remover = new Remove();
		remover.setAttributeIndices("first");
		remover.setInvertSelection(false);
		
		remover.setInputFormat(originTrain);
		Instances train = Filter.useFilter(originTrain, remover);
		
		//load data to predict (target data)
		String fileInput = "predict-week-"+week+".csv";
		
		CSVLoader predictLoader = new CSVLoader();
		String[] predOptions;
		
		if(hasActualClass)
		{
			predOptions = new String[6];
			predOptions[0]="-N";
			predOptions[1]="last";
			predOptions[2]="-L";
			predOptions[3]="last:Green,Yellow,Red";
			predOptions[4]="-S";
			predOptions[5]="first";
		}
		else
		{
			predOptions = new String[2];
			predOptions[0]="-S";
			predOptions[1]="first";
		}
			
		predictLoader.setOptions(predOptions);
		predictLoader.setSource(new File(path+folderInput+fileInput));
		Instances originPredictData = predictLoader.getDataSet();
		
		Instances newOriginPredictData = new Instances(originPredictData);
		if(!hasActualClass)
		{
			Add filter = new Add();
			filter.setAttributeIndex("last");
			filter.setAttributeName("signal");
			filter.setInputFormat(newOriginPredictData);
			newOriginPredictData = Filter.useFilter(newOriginPredictData, filter);
		}
		
		newOriginPredictData.setClassIndex(newOriginPredictData.numAttributes()-1);
													
		//remove unnecessary attributes (att starts from 1)
//		Remove predRemover = new Remove();
//		if(hasActualClass)
//			predRemover.setAttributeIndices("first,last");
//		else
//			predRemover.setAttributeIndices("first,last");
//			
//		predRemover.setInvertSelection(false);
//		
//		predRemover.setInputFormat(originPredictData);
//		Instances predictData = Filter.useFilter(originPredictData, predRemover);
		Instances predictData = Filter.useFilter(newOriginPredictData, remover);
		
		//Standardize target data using train data's statistics
		Standardize standardizer = new Standardize();
		standardizer.setInputFormat(train);
		Instances standardPredictData = Filter.useFilter(predictData, standardizer);
		
		//load model
		String fileModel = "logReg-week-"+week+".model";
		Logistic logReg = (Logistic) SerializationHelper.read(folderModel+fileModel);
		
		//start prediction
		String outputFile = "prediction-result-"+lacourse+"-week-"+week+".csv";
				
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(folderPredictionName+outputFile)));
		
		if(hasActualClass)
			out.write("npm,la_course,week,actual,prediction\n");
		else
			out.write("npm,la_course,week,prediction\n");
		
		
		for(int i=0;i<standardPredictData.numInstances();i++)
		{
			Instance instPredict = standardPredictData.instance(i);
			
			String npm = originPredictData.instance(i).attribute(0).value(i);
			
			double prediction = logReg.classifyInstance(instPredict);
			String predString = originTrain.classAttribute().value((int)prediction);

			if(hasActualClass)
			{
				double actualClass = standardPredictData.instance(i).classValue();
				String actualString = standardPredictData.classAttribute().value((int)actualClass);
				
				out.write(npm+","+lacourse+","+week+","+actualString+","+predString+"\n");
			}
			else
			{
				out.write(npm+","+lacourse+","+week+","+predString+"\n");
			}
		}
		
		out.close();

	}
}
