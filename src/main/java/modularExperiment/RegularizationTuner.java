package modularExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;

public class RegularizationTuner 
{
	private Instances dataset;
	private HashMap<String,Double> weights;
	
	public RegularizationTuner(Instances dataset)
	{
		this.dataset = dataset;
		
		weights = new HashMap<String,Double>();
		weights.put("Red", 1.5);
		weights.put("Yellow", 1.2);
		weights.put("Green", 1.0);
	}
	
	public double tuneParamLambda(String path, String info) throws Exception
	{
		
		String outputFile = path + File.separator+"RegularizationTuning-"+info+".csv" ;
		String outputMat = path + File.separator+"RegularizationTuningMatrix-"+info+".txt";
		String outputChart = path + File.separator+ "RegularizationTuningChart-"+info+".csv";
		
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outputFile)));
		BufferedWriter outMat = new BufferedWriter(new FileWriter(new File(outputMat)));
		BufferedWriter outChart = new BufferedWriter(new FileWriter(new File(outputChart)));
		
		out.write("Lambda,Accuracy(%),F"+dataset.classAttribute().value(0)+",F"+dataset.classAttribute().value(1)+",F"+dataset.classAttribute().value(2)+",FMean,Score\n");
		outChart.write("Lambda,Accuracy,F"+dataset.classAttribute().value(0)+",F"+dataset.classAttribute().value(1)+",F"+dataset.classAttribute().value(2)+",FMean,Score\n");
		
		double weight1 = weights.get(dataset.classAttribute().value(0));
		double weight2 = weights.get(dataset.classAttribute().value(1));
		double weight3 = weights.get(dataset.classAttribute().value(2));
		double weightAcc = 1.2;
		double weightMeanF = 1.0;
		
		double bestLambda = Double.MIN_VALUE;
		double bestScoreL = Double.MIN_VALUE;
				
		double accBefore=-1.0;
		double f1Before=-1.0;
		double f2Before=-1.0;
		double f3Before=-1.0;
		
		for(int i=0;i<=700;i++)
		{			
			if(i%100==0)
				System.out.println(i);
			
			
			double lambda = i*0.01;
			
			Logistic log = new Logistic();
			log.setRidge(lambda);			
			
			//randoming data
			int folds = 10;
						
			//stratify
			if(dataset.classAttribute().isNominal())
				dataset.stratify(folds);
			
			double meanAcc = 0;
			double meanF1 = 0;
			double meanF2 = 0;
			double meanF3 = 0;
			double meanFMean =0;
			double scoreL = 0;
			
			Evaluation eval10 = new Evaluation(dataset);
			for(int f=0; f<folds; f++)
			{
				Instances trainCV = dataset.trainCV(folds, f);
				Instances testCV = dataset.testCV(folds, f);
				
				log.buildClassifier(trainCV);
				
				Evaluation eval = new Evaluation(dataset);
								
				eval.evaluateModel(log, testCV);
				eval10.evaluateModel(log,testCV);
								
				meanAcc+=eval.pctCorrect();
				meanF1 += eval.fMeasure(0);
				meanF2 += eval.fMeasure(1);
				meanF3 += eval.fMeasure(2);
				meanFMean += ((eval.fMeasure(0)+eval.fMeasure(1)+eval.fMeasure(2))/3.0);
			}
			
			meanAcc=meanAcc/folds;
			meanF1=meanF1/folds;
			meanF2=meanF2/folds;
			meanF3=meanF3/folds;
			meanFMean=meanFMean/folds;
				
			scoreL = weight1*meanF1 + weight2*meanF2 + weight3*meanF3 + weightAcc*(meanAcc/100.0) + weightMeanF*meanFMean;	
			
			if(scoreL > bestScoreL)
			{
				bestScoreL = scoreL;
				bestLambda = lambda;
			}
			
			if(!(accBefore==meanAcc && f1Before==meanF1 && f2Before==meanF2 && f3Before==meanF3 ))
			{	
				out.write(lambda+","+meanAcc+","+meanF1+","+meanF2+","+meanF3+","+meanFMean+","+scoreL+"\n");
					
				outMat.write(eval10.toMatrixString("Lambda = "+lambda));
				outMat.write("\n");
				
				outChart.write(lambda+","+(meanAcc/100.0)+","+meanF1+","+meanF2+","+meanF3+","+meanFMean+","+scoreL+"\n");
			}
			
			accBefore=meanAcc;
			f1Before=meanF1;
			f2Before=meanF2;
			f3Before=meanF3;
		}
		
		out.write("best Lambda,"+bestLambda+"\n");
		outChart.write("best Lambda,"+bestLambda+"\n");
		out.close();
		outMat.close();
		outChart.close();
				
		return bestLambda;
	}
}
