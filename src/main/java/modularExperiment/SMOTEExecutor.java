package modularExperiment;

import weka.core.AttributeStats;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.unsupervised.instance.Randomize;

public class SMOTEExecutor 
{
	private Instances dataset;
	
	public SMOTEExecutor(Instances dataset)
	{
		this.dataset = dataset;
	}
	
	public Instances applySMOTE(int s) throws Exception
	{
		//find the majority class
		AttributeStats stats = dataset.attributeStats(dataset.classIndex());
		int[] classMember = stats.nominalCounts;
		
		int majorClass = 0;
			
		for(int i=1;i<classMember.length;i++)
		{
			if(classMember[i]>classMember[majorClass])
			{
				majorClass=i;				
			}
		}
				
		SMOTE smoter = new SMOTE();
		smoter.setRandomSeed(161803398);
		smoter.setNearestNeighbors(s);
		
		for(int i=0;i<classMember.length;i++)
		{
			if(i!=majorClass)
			{
				double percentage = ((double)classMember[majorClass]/(double)classMember[i]-1.0)*100.0;
				//System.out.println(percentage);
				smoter.setInputFormat(dataset);
				smoter.setPercentage(percentage);
				smoter.setClassValue((i+1)+"");
				dataset = Filter.useFilter(dataset, smoter);
			}
		}
		
		Randomize randomizer = new Randomize();
		randomizer.setRandomSeed(161803398);
		randomizer.setInputFormat(dataset);
		Instances smotedDataset = Filter.useFilter(dataset, randomizer);
		
		return smotedDataset;
	}
}
