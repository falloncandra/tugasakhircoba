package modularExperiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;


public class SMOTETuner 
{
	private Instances dataset;
	private HashMap<String,Double> weights;
	
	public SMOTETuner(Instances dataset)
	{
		this.dataset = dataset;
		
		weights = new HashMap<String,Double>();
		weights.put("Red", 1.5);
		weights.put("Yellow", 1.2);
		weights.put("Green", 1.0);
	}
	
	public int tuneParamS(String path, String info) throws Exception
	{		
		
		String outputFile = path + File.separator +"SmoteTuning-"+info+".csv" ;
		String outputMat = path + File.separator +"SmoteTuningMatrix-"+info+".txt";
		String outputChart = path + File.separator +"SmoteTuningChart-"+info+".csv";
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(outputFile)));
		BufferedWriter outMat = new BufferedWriter(new FileWriter(new File(outputMat)));
		BufferedWriter outChart = new BufferedWriter(new FileWriter(new File(outputChart)));
		
		out.write("S,Accuracy(%),F"+dataset.classAttribute().value(0)+",F"+dataset.classAttribute().value(1)+",F"+dataset.classAttribute().value(2)+",FMean,ScoreS\n");
		out.write("S,Accuracy,F"+dataset.classAttribute().value(0)+",F"+dataset.classAttribute().value(1)+",F"+dataset.classAttribute().value(2)+",FMean,ScoreS\n");
		
		int s=1;
		double weight1 = weights.get(dataset.classAttribute().value(0));
		double weight2 = weights.get(dataset.classAttribute().value(1));
		double weight3 = weights.get(dataset.classAttribute().value(2));
		double weightAcc = 1.2;
		double weightMeanF = 1.0;
		
		int bestS = Integer.MIN_VALUE;
		double bestScoreS = Double.MIN_VALUE;
		
		try{
			while(true)
			{
				if(s%10==0)
					System.out.println("s =" + s);
				
				Logistic log = new Logistic();
				log.setRidge(0);
				
				//randoming data
				int folds = 10;
							
				//stratify
				if(dataset.classAttribute().isNominal())
					dataset.stratify(folds);
				
				double meanAcc = 0;
				double meanF1 = 0; //green
				double meanF2 = 0; //yellow
				double meanF3 = 0; //red
				double meanFMean = 0;
				double scoreS = 0;
										
				Evaluation eval10=new Evaluation(dataset);
				
				for(int f=0; f<folds; f++)
				{
					Instances trainCV = dataset.trainCV(folds, f);
					Instances testCV = dataset.testCV(folds, f);
					
					SMOTEExecutor SmoteExe = new SMOTEExecutor(trainCV);
					Instances smotedData = SmoteExe.applySMOTE(s);
										
					log.buildClassifier(smotedData);
					
					Evaluation eval = new Evaluation(dataset);
					
					eval.evaluateModel(log, testCV);
					eval10.evaluateModel(log, testCV);
									
					meanAcc+=eval.pctCorrect();
					meanF1 += eval.fMeasure(0);
					meanF2 += eval.fMeasure(1);
					meanF3 += eval.fMeasure(2);
					meanFMean += ((eval.fMeasure(0)+eval.fMeasure(1)+eval.fMeasure(2))/3.0);
				}
				
				meanAcc=meanAcc/folds;
				meanF1=meanF1/folds; //green
				meanF2=meanF2/folds; //yellow
				meanF3=meanF3/folds; //red
				meanFMean=meanFMean/folds;		
				
				scoreS = weight1*meanF1 + weight2*meanF2 + weight3*meanF3 + weightAcc*(meanAcc/100.0) + weightMeanF*meanFMean;	
				
				if(scoreS > bestScoreS)
				{
					bestScoreS = scoreS;
					bestS = s;
				}
				
				out.write(s+","+meanAcc+","+meanF1+","+meanF2+","+meanF3+","+meanFMean+","+scoreS+"\n");						
				outMat.write(eval10.toMatrixString("S = "+s));
				outMat.write("\n");
				outChart.write(s+","+(meanAcc/100.0)+","+meanF1+","+meanF2+","+meanF3+","+meanFMean+","+scoreS+"\n");
				
				s++;
			}		
		}
		catch(Exception e)
		{
			//break from loop and do nothing
			System.out.println("Max Neighbor : "+(s-1));
		}
		
		out.write("best S,"+bestS+"\n");
		outChart.write("best S,"+bestS+"\n");
		out.close();
		outMat.close();
		outChart.close();
				
		return bestS;
		
	}
}
